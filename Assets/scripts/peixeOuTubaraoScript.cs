﻿using UnityEngine;
using System.Collections;

public class peixeOuTubaraoScript : MonoBehaviour
{

    private bool ehPeixe;
    private volatile bool foiPego;
    private bool ficouInvisivel;
    private BoxCollider2D colliderAnzol;
    private PolygonCollider2D colliderPeixe;

    // Use this for initialization
    void Start()
    {
        if (gameObject.name.Contains("peixe"))
        {
            ehPeixe = true;
        }
        else
        {
            ehPeixe = false;
        }
        foiPego = false;
        ficouInvisivel = false;

        colliderPeixe = gameObject.GetComponent<PolygonCollider2D>();
        colliderAnzol = GameObject.Find("fish_hook").GetComponent<BoxCollider2D>();

    }

    public void setPeixeFoiPego(bool novoValor)
    {
        foiPego = novoValor;
    }

    public bool getPeixeFoiPego()
    {
        return this.foiPego;
    }

    // Update is called once per frame
    void Update()
    {
        if (ficouInvisivel == false && foiPego == false)
        {
            Vector3 posicaoObjetoMoverPraFrente = gameObject.transform.position;
            if(ehPeixe == true && foiPego == false)
            {
                gameObject.transform.position = new Vector3(posicaoObjetoMoverPraFrente.x + 0.1f, posicaoObjetoMoverPraFrente.y, posicaoObjetoMoverPraFrente.z);
            }
            else 
            {
                gameObject.transform.position = new Vector3(posicaoObjetoMoverPraFrente.x + 0.2f, posicaoObjetoMoverPraFrente.y, posicaoObjetoMoverPraFrente.z);
            }
            
        }
        if (ficouInvisivel == true)
        {
            Debug.Log("peixeOuTubaraoScript.cs// peixe ou tubarão ficou invisível e vai ser destruído");
            if(foiPego == true)
            {
                minijogo_pegar_peixe minijogo = GameObject.Find("background_minijogo_pegar_sushi").GetComponent<minijogo_pegar_peixe>();
                minijogo.aumentarQuantosPeixesPegou();
            }
            Destroy(gameObject);
        }

    }

    void OnBecameInvisible()
    {
        this.ficouInvisivel = true;
    }

    void OnCollisionStay2D(Collision2D collisionInfo)
    {
        //Debug.Log("peixeOuTubaraoScript.cs.cs// peixe ou tubarão colidindo!");
    }
}
