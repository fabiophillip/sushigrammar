﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class cumbuca_script : MonoBehaviour {

    private Sprite cumbucaHighlight;
    private Sprite cumbucaNormal;
    private Sprite cumbucaBlock;
    private int quantosSushisEmCimaDaCumbuca;
    private LinkedList<GameObject> sushisNaCumbuca;
    private GameObject ultimoSushiMovido;

	// Use this for initialization
	void Start () {
        Texture2D texture = Resources.Load("cumbuquinha_sushi_highlighted") as Texture2D;
        cumbucaHighlight = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));

        Texture2D texture2 = Resources.Load("cumbuquinha_sushi") as Texture2D;
        cumbucaNormal = Sprite.Create(texture2, new Rect(0.0f, 0.0f, texture2.width, texture2.height), new Vector2(0.5f, 0.5f));
        
        Texture2D texture3 = Resources.Load("cumbuquinha_sushi_block") as Texture2D;
        cumbucaBlock = Sprite.Create(texture3, new Rect(0.0f, 0.0f, texture3.width, texture3.height), new Vector2(0.5f, 0.5f));


        quantosSushisEmCimaDaCumbuca = 0;
        this.sushisNaCumbuca = new LinkedList<GameObject>();


    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetMouseButtonUp(0) == true)
        {
            //usuário despressionou botão do mouse
            //usuario levantou bbotão do mouse, cumbuca deixa de ser highlighted
            //Debug.Log("cumbuca_script.cs//usuario levantou botao mouse");
            GetComponent<SpriteRenderer>().sprite = cumbucaNormal;
            /*if(ultimoSushiMovido != null)
            {
                if(quantosSushisEmCimaDaCumbuca == 1)
                {
                    GameObject dummySushi1 = GameObject.Find("dummy_zushi_cumbuca1");
                    ultimoSushiMovido.transform.position = dummySushi1.transform.position;
                    ultimoSushiMovido.GetComponent<sushi_script>().setPosicaoInicialSushi(dummySushi1.transform.position);
                }
                else if(quantosSushisEmCimaDaCumbuca == 2)
                {
                    GameObject dummySushi2 = GameObject.Find("dummy_zushi_cumbuca2");
                    ultimoSushiMovido.transform.position = dummySushi2.transform.position;
                    ultimoSushiMovido.GetComponent<sushi_script>().setPosicaoInicialSushi(dummySushi2.transform.position);
                }
                else if(quantosSushisEmCimaDaCumbuca == 3)
                {
                    GameObject dummySushi3 = GameObject.Find("dummy_zushi_cumbuca3");
                    ultimoSushiMovido.transform.position = dummySushi3.transform.position;
                    ultimoSushiMovido.GetComponent<sushi_script>().setPosicaoInicialSushi(dummySushi3.transform.position);
                }
                else
                {
                    GameObject dummySushi4 = GameObject.Find("dummy_zushi_cumbuca4");
                    ultimoSushiMovido.transform.position = dummySushi4.transform.position;
                    ultimoSushiMovido.GetComponent<sushi_script>().setPosicaoInicialSushi(dummySushi4.transform.position);
                }
            }*/
        }
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("cumbuca_script.cs// novo sushi entrou na cumbuca! -- trigger enter");
        if (col.name.StartsWith("sushi_") == true)
        {
            //objeto que colidiu é um novo sushi!
            if(quantosSushisEmCimaDaCumbuca < 4)
            {
                GameObject objetoSushiColidiu = col.gameObject;
                sushi_script sushiColidiuComBarcaca = objetoSushiColidiu.GetComponent<sushi_script>();
                sushiColidiuComBarcaca.setSushiEmCimaDeBarcoOuCumbuca(true);
                sushisNaCumbuca.AddLast(objetoSushiColidiu);
            }
            
            if(Input.GetMouseButton(0) == true)
            {
                //botão do mouse está sendo pressionado
                if(quantosSushisEmCimaDaCumbuca < 4)
                {
                    //Debug.Log("barco_sushi.cs//tem menos sushis");
                    quantosSushisEmCimaDaCumbuca = quantosSushisEmCimaDaCumbuca + 1;
                    GetComponent<SpriteRenderer>().sprite = cumbucaHighlight;
                    ultimoSushiMovido = col.gameObject;
                }
                else
                {
                    //Debug.Log("barco_sushi.cs//tem mais  sushis");
                    GetComponent<SpriteRenderer>().sprite = cumbucaBlock;
                }
                
            }
            
            
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        //print("Are you even colliding my friend?");
        if (col.name.StartsWith("sushi_") == true && sushiEstavaNaCumbuca(col.gameObject) == true)
        {
            //Debug.Log("cumbuca_script.cs// novo sushi saiu da cumbuca! -- trigger enter");
            //objeto que colidiu é um sushi!
            quantosSushisEmCimaDaCumbuca = quantosSushisEmCimaDaCumbuca - 1;
            sushi_script sushiColidiuComBarcaca = col.gameObject.GetComponent<sushi_script>();
            sushiColidiuComBarcaca.setSushiEmCimaDeBarcoOuCumbuca(false);

            //tem de remover o sushi da lista de sushis no barco
            GameObject sushiRemover = col.gameObject;
            sushisNaCumbuca.Remove(sushiRemover);
        }
        GetComponent<SpriteRenderer>().sprite = cumbucaNormal;
    }

    private bool sushiEstavaNaCumbuca(GameObject sushi)
    {
        bool sushiEstahNaCumbuca = false;
        foreach (var sushiNoBarco in sushisNaCumbuca)
        {
            if (sushiNoBarco == sushi)
            {
                sushiEstahNaCumbuca = true;
                break;
            }
        }
        return sushiEstahNaCumbuca;
    }
    
    void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().sprite = cumbucaNormal;

    }

}
