﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class sushi_script : MonoBehaviour
{
    private bool arrastandoObjeto = false;
    private float distance;
    private Vector3 posicaoInicialSushi;//essa posição vai mudar quando o sushi for posicionado na barcaca ou  na cumbuca
    private bool sushiEmCimaDeBarcoOuCumbuca;//se o sushi está em cima de cumbuca ou barco,ele não volta pro seu canto quando o mouse é soltado
    private bool sushiEmCimaDeBarco;
    private GameObject textoOrdemDoSushi;
    private Vector3 posicaoInicialSushiNaChapa;//a posição inicial msm do sushi(na chapa)
    private barco_sushi barcoQueSushiFicaEmCima;//o barco que o sushi fica em cima
    private bool sushiMoveuDeBarcoParaBarcoOuBarcoParaCumbuca;//se ele se mexeu nesse sentido, não precisa spawnear um novo sushi no lugar dele

    public barco_sushi getBarcoQueSushiFicaEmCima()
    {
        return barcoQueSushiFicaEmCima;
    }

    public Vector3 getPosicaoInicialSushi()
    {
        return posicaoInicialSushi;
    }

    public Vector3 getPosicaoInicialSushiNaChapa()
    {
        return posicaoInicialSushiNaChapa;
    }

    public void setPosicaoInicialSushi(Vector3 novoValor)//chamado somente pelas cumbucas
    {
        posicaoInicialSushi = novoValor;
    }

    public void setSushiEmCimaDeBarco(bool novoValor, barco_sushi barcoAssociado)
    {
        sushiEmCimaDeBarco = novoValor;
        if(sushiEmCimaDeBarco == true)
        {
            //sushi em cima de barco? a ordem dele deve mudar!
            textoOrdemDoSushi.GetComponent<Text>().enabled = true;
            barcoQueSushiFicaEmCima = barcoAssociado;
        }
        else
        {
            textoOrdemDoSushi.GetComponent<Text>().enabled = false;
            barcoQueSushiFicaEmCima = null;
        }
    }

    public bool getSushiEmCimaDeBarco()
    {
        return sushiEmCimaDeBarco;
    }




    void OnMouseDown()
    {
        distance = Vector3.Distance(transform.position, Camera.main.transform.position);
        arrastandoObjeto = true;
        //objeto se mexendo tem de ficar em uma layer maior, pra não ficar por debaixo de nenhum elemento
        GameObject canvasLayerSuperior = GameObject.Find("Canvas_sushi_na_frente");
        transform.parent = canvasLayerSuperior.transform;
        //também tem de aumentar a order in layer do sprite do sushi pra ficar que nem a do pai
        GetComponent<Renderer>().sortingOrder = 5;
        //Debug.Log("erro pause consertado");
    }

    

    void OnMouseUp()
    {
        arrastandoObjeto = false;
        //objeto parou de se mexer tem de ficar em uma layer menor, pra não ficar por cima de outros objetos que o user arrastar no futuro
        GameObject canvasLayerSuperior = GameObject.Find("Canvas_sushi_texto_sushi");
        transform.parent = canvasLayerSuperior.transform;
        //também tem de reduzir a order in layer do sprite do sushi pra ficar que nem a do pai
        GetComponent<Renderer>().sortingOrder = 3;
        if (sushiEmCimaDeBarcoOuCumbuca == false)
        {
            this.transform.position = posicaoInicialSushi;
        }
        else
        {
            //primeiro vamos checar: sushi se moveu da chapa para outra posição ou foi de barco/cumbuca para outra posição?
            //se ele se moveu da chapa, podemos criar outro sushi no lugar dele
            if(posicaoInicialSushi == posicaoInicialSushiNaChapa)
            {
                //sushi se moveu da chapa para um barco ou cumbuca
                this.sushiMoveuDeBarcoParaBarcoOuBarcoParaCumbuca = false;
            }
            else
            {
                //sushi moveu de barco para barco ou de barco para cumbuca
                this.sushiMoveuDeBarcoParaBarcoOuBarcoParaCumbuca = true;
            }
            
            this.posicaoInicialSushi = this.transform.position;
            
            
            if (sushiMoveuDeBarcoParaBarcoOuBarcoParaCumbuca == true)
            {
                // sushi se moveu de barco pra barco ou de barco pra cumbuca, então não precisa gerar um novo sushi
                //Debug.Log("sushi_script//um sushi novo não é necessário");
            }
            else
            {
                //podemos criar um novo sushi na posição do que se mexeu. agora, tem sushi na reserva o suficiente?
                contador_sushis_script contadorDeSushis = GameObject.Find("contador_sushis").GetComponent<contador_sushis_script>();
                int quantosSushisNaReserva = contadorDeSushis.getQuantosSushisTemNaReserva();
                if(quantosSushisNaReserva > 0)
                {
                    // spot is empty and we have enough sushi, we can spawn new sushi
                    //Debug.Log("sushi_script//um sushi novo É necessário");
                    //criar um novo sushi
                    GameObject novoSushi = (GameObject)Instantiate(gameObject, posicaoInicialSushiNaChapa, transform.rotation);
                    //novo sushi tem de estar visível
                    novoSushi.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1.0f);
                    novoSushi.transform.parent = transform.parent;
                    novoSushi.name = "sushi_" + contador_sushis_script.idNovosSushisDoJogo;
                    novoSushi.tag = "sushi_object";
                    novoSushi.transform.localScale = new Vector3(73.97274f, 73.97274f);
                    foreach (Transform child in novoSushi.transform)
                    {
                        string antigoNomeFilhoHierarquico = child.name;
                        string[] splitNomeFilhoHierarquico = antigoNomeFilhoHierarquico.Split('_');
                        string novoNomeFilhoHierarquico = "";
                        for (int i = 0; i < splitNomeFilhoHierarquico.Length - 1; i++)
                        {
                            novoNomeFilhoHierarquico = novoNomeFilhoHierarquico + splitNomeFilhoHierarquico[i] + "_";
                        }
                        novoNomeFilhoHierarquico = novoNomeFilhoHierarquico + contador_sushis_script.idNovosSushisDoJogo;
                        child.name = novoNomeFilhoHierarquico;
                        if (novoNomeFilhoHierarquico.Contains("ordem_"))
                        {
                            child.GetComponent<Text>().enabled = false;
                        }

                    }
                    //e falta setar um novo texto para esse novo sushi
                    GameObject bancoDeDados = GameObject.Find("banco_de_dados");
                    banco_de_dados banco = bancoDeDados.GetComponent<banco_de_dados>();

                    //esse texto não deve ser o mesmo do sushi que acabou de ser puxado, então vamos ter de pegar a partícula dele primeiro
                    string nomeObjeto = gameObject.transform.name;
                    string[] nome_splitado = nomeObjeto.Split('_');
                    string numeroDoSushi = nome_splitado[nome_splitado.Length - 1];
                    string particulaAntigoSushi = GameObject.Find("texto_sushi_" + numeroDoSushi).GetComponent<Text>().text;

                    banco.mudarTextoDoSushiSemRepetido(contador_sushis_script.idNovosSushisDoJogo, particulaAntigoSushi );

                    contador_sushis_script.idNovosSushisDoJogo = contador_sushis_script.idNovosSushisDoJogo + 1;
                    //falta também decrementar quantos sushis tem na reserva
                    contadorDeSushis.decrementarQuantosSushisNaReserva(1);
                    
                }
                else
                {
                    contadorDeSushis.addPosicaoSemSushi(posicaoInicialSushiNaChapa);
                }

            }
        }
       
    }

    public void setSushiEmCimaDeBarcoOuCumbuca(bool novovalor)
    {
        sushiEmCimaDeBarcoOuCumbuca = novovalor;
    }


    bool objectsOverlap(Transform t, Transform t2 )
    {
        bool result = false;

        Rect pos = new Rect(t.position.x - (t.localScale.x / 2), t.position.z - (t.localScale.z / 2), t.localScale.x, t.localScale.z);
        Rect pos1 = new Rect(t2.position.x - (t2.localScale.x / 2), t2.position.z - (t2.localScale.z / 2), t2.localScale.x, t2.localScale.z);
        result = pos.Contains(new Vector2(t2.position.x, t2.position.z));
        return result;
    }

    void Update()
    {
        //Debug.Log("sushi_script//arrastandoObjeto = " + arrastandoObjeto);
        if (arrastandoObjeto == true)
        {
            //Debug.Log("sushi_script.cs//arrastando_objeto=" + arrastandoObjeto);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector3 rayPoint = ray.GetPoint(distance);
            transform.position = rayPoint;
            if(sushiEmCimaDeBarco == true && arrastandoObjeto == true)
            {
                barcoQueSushiFicaEmCima.atualizarPosicaoTodosOsSushisNoBarco();
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        posicaoInicialSushi = transform.position;
        sushiEmCimaDeBarcoOuCumbuca = false;
        sushiEmCimaDeBarco = false;
        //tem que achar os textos de ordem do sushi que ficam em cima deles e torná-los invisíveis
        string nomeObjeto = gameObject.transform.name;
        string[] nome_splitado = nomeObjeto.Split('_');
        string numeroDoSushi = nome_splitado[nome_splitado.Length - 1];
        string nomeTextoObjetoOrdemSushi = "ordem_sushi_" +numeroDoSushi; 
        textoOrdemDoSushi = GameObject.Find(nomeTextoObjetoOrdemSushi);
        posicaoInicialSushiNaChapa = transform.position;
        //Debug.Log("sushi_script//posicao inicial sushi na chapa:" + posicaoInicialSushiNaChapa);
        sushiMoveuDeBarcoParaBarcoOuBarcoParaCumbuca = false;


    }

    //sushi fica em cima do barco. Sua ordem no barco vai mudar. esse método é chamado pelo barco_sushi.cs que o sushi está em cima para atualizar sua posição
    public void setOrdemSushiNoBarco(int novaOrdem)
    {
        if(novaOrdem == 1)
        {
            textoOrdemDoSushi.GetComponent<Text>().text = "一";
        }
        else if (novaOrdem == 2)
        {
            textoOrdemDoSushi.GetComponent<Text>().text = "二";
        }
        else if (novaOrdem == 3)
        {
            textoOrdemDoSushi.GetComponent<Text>().text = "三";
        }
        else 
        {
            textoOrdemDoSushi.GetComponent<Text>().text = "四";
        }

    }

    public bool sushiEstahNaChapa()
    {
        if(this.sushiEmCimaDeBarcoOuCumbuca == false && arrastandoObjeto == false)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


}
	
	
