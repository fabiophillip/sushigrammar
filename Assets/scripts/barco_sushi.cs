﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;

public class barco_sushi : MonoBehaviour {
    
    shoyu_ok botaoOkAssociadoAoBarco;
    int quantosSushisEmCimaDoBarco;
    int quantosSushisNecessariosPraFrase;
    private Sprite barcoHighlight;
    private Sprite barcoHighlightBlock;//quando o barco estah cheio, não podemos inserir novos sushis
    private Sprite barcoNormal;
    private LinkedList<GameObject> sushisEmCimaDeBarco;
    private bool fazerSumirBarco;
    private Vector3 posicaoInicialBarco;
    private bool sushisEstaoTransparentes;
    private volatile bool barcoSumindo;
    private int numBarcoSushi;

    public void setQuantosSushisNecessariosPraFrase(int novoValor)
    {
        quantosSushisNecessariosPraFrase = novoValor;
        //bom, issoo tem um novo valor então não devia ter mais sushis no barco
        quantosSushisEmCimaDoBarco = 0;
        sushisEmCimaDeBarco = new LinkedList<GameObject>();
    }

    // Use this for initialization
    void Start()
    {
        quantosSushisEmCimaDoBarco = 0;
        //falta carregar as imagens do barco normal e highlighted
        
        Texture2D texture = Resources.Load("barcodestacado") as Texture2D;
        
        barcoHighlight = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        
        Texture2D texture2 = Resources.Load("sushi_boat") as Texture2D;
          barcoNormal = Sprite.Create(texture2, new Rect(0.0f, 0.0f, texture2.width, texture2.height), new Vector2(0.5f, 0.5f));

       
        Texture2D texture3 = Resources.Load("barcodestacadobloqueado") as Texture2D;
       barcoHighlightBlock = Sprite.Create(texture3, new Rect(0.0f, 0.0f, texture3.width, texture3.height), new Vector2(0.5f, 0.5f));


        //quais os sushis em cima do barco no começo? nenhum. então inicia a linkedList
        this.sushisEmCimaDeBarco = new LinkedList<GameObject>();

        //falta achar o botão ok associado ao barco
        string nomeObjeto = gameObject.transform.name;
        string[] nome_splitado = nomeObjeto.Split('_');
        string numeroDoBarco = nome_splitado[nome_splitado.Length - 1];
        numBarcoSushi = int.Parse(numeroDoBarco);
        string nomeBotaoOkDoBarco = "checked_button_boat_" + numeroDoBarco;
        GameObject objetoBotaoOkAssociadoAoBarco = GameObject.Find(nomeBotaoOkDoBarco);
        
        this.botaoOkAssociadoAoBarco = objetoBotaoOkAssociadoAoBarco.GetComponent<shoyu_ok>();

        fazerSumirBarco = false;
        barcoSumindo = false;
        posicaoInicialBarco = gameObject.transform.position;

        sushisEstaoTransparentes = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0) == true)
        {
            //usuário despressionou botão do mouse
            //usuario levantou bbotão do mouse, cumbuca deixa de ser highlighted
            GetComponent<SpriteRenderer>().sprite = barcoNormal;
        }
        if(fazerSumirBarco == true)
        {
            fazerSumirBarco = false;
            barcoSumindo = true;
            StartCoroutine(fazerOBarcoDesaparecer());   
        }
    }

    public bool getBarcoSumindo()
    {
        return barcoSumindo;
    }
    public bool getBarcoCheio()
    {
        if(quantosSushisEmCimaDoBarco >= quantosSushisNecessariosPraFrase)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    Vector3 posicaoObjetoMoverUp;
    IEnumerator fazerOBarcoDesaparecer()
    {
        //Debug.Log("barco_sushi.cs//fazer o barco desaparecer iniciado");
        yield return new WaitForSeconds(6.0f);
        //Debug.Log("barco_sushi.cs//the wait is over");
        //fazer o barco e seu texto passarem pra frente
        string nomeUmObjetoBarco = gameObject.name;
        string[] nomeUmObjetoBarcoSplitado = nomeUmObjetoBarco.Split('_');
        string numeroUmObjetoBarco = nomeUmObjetoBarcoSplitado[nomeUmObjetoBarcoSplitado.Length - 1];
        string nomeObjetoTextoAcompanhaBarco = "frase_sushi_boat_" + numeroUmObjetoBarco;
        GameObject objetoTextoAcompanhaBarco = GameObject.Find(nomeObjetoTextoAcompanhaBarco);

        //objeto se mexendo tem de ficar em uma layer maior, pra não ficar por debaixo de nenhum elemento
        GameObject canvasLayerSuperior = GameObject.Find("Canvas_sushi_na_frente");
        transform.parent = canvasLayerSuperior.transform;
        //também tem de aumentar a order in layer do sprite do barco pra ficar que nem a do pai
        GetComponent<Renderer>().sortingOrder = 4;
        objetoTextoAcompanhaBarco.transform.parent = canvasLayerSuperior.transform;

        //botar os objetos em uma lista de objetos pra mover
        LinkedList<GameObject> objetosMoverPraCima = new LinkedList<GameObject>();
        objetosMoverPraCima.AddLast(gameObject);

        GameObject canvasLayerSuperiorSushis = GameObject.Find("Canvas_sushi_na_frente_frente");
        //fazer aparecer os sushis de novo 
        foreach ( var sushiNoBarco in sushisEmCimaDeBarco)
        {
            string nomeUmObjetoSushi = sushiNoBarco.name;
            string[] nomeSushi_splitado = nomeUmObjetoSushi.Split('_');
            string numeroSushi = nomeSushi_splitado[nomeSushi_splitado.Length - 1];
            string nomeObjetoTextoSushi = "texto_sushi_" + numeroSushi;
            GameObject objetoTextoSushi = GameObject.Find(nomeObjetoTextoSushi);
            sushiNoBarco.GetComponent<Renderer>().enabled = true;
            objetoTextoSushi.GetComponent<Text>().enabled = true;
            sushiNoBarco.transform.parent = canvasLayerSuperiorSushis.transform;
            sushiNoBarco.GetComponent<Renderer>().sortingOrder = 7;

            objetosMoverPraCima.AddLast(sushiNoBarco);
        }

        //fazer os objetos moverem pra cima
        //Debug.Log("barco_sushi.cs//fazer objetos moverem pra cima iniciado");
        int contador = 0;
        while (contador < 20)
        {
            foreach (var objetoMoverPraCima in objetosMoverPraCima)
            {
                //Debug.Log("barco_suhsi.cs//objeto mover pra cima=" + objetoMoverPraCima.name);
                posicaoObjetoMoverUp = objetoMoverPraCima.transform.position;
                objetoMoverPraCima.transform.position = new Vector3(posicaoObjetoMoverUp.x, posicaoObjetoMoverUp.y + 1, posicaoObjetoMoverUp.z );
            }

            contador = contador + 1;
            yield return new WaitForSeconds(.100f);
           //Debug.Log("barco_sushi.cs//contador = " + contador);
        }
        
        
        //Debug.Log("barco_sushi.cs//objetos pararam de se mexer!");
        //destruir os sushis em cima do barco
        foreach( var sushiEmCimaDoBarco in sushisEmCimaDeBarco)
        {
            if(sushiEmCimaDoBarco != null)
            {
                Destroy(sushiEmCimaDoBarco);
            }
            
        }

        //barco agora não tem mais sushis em cima
        this.sushisEmCimaDeBarco = new LinkedList<GameObject>();
        this.quantosSushisEmCimaDoBarco = 0;

        //faltou só trocar a frase do barco
        GameObject objetoBancoDeDados = GameObject.Find("banco_de_dados");
        banco_de_dados geraNovaFraseBarco = objetoBancoDeDados.GetComponent<banco_de_dados>();
        int numeroEmIntDoObjetoBarco = int.Parse(numeroUmObjetoBarco);
        geraNovaFraseBarco.mudarTextoDaBarcaca(numeroEmIntDoObjetoBarco);



        //e mover o barco de volta ao seu lugar gradativamente
        while (gameObject.transform.position.y > posicaoInicialBarco.y)
        {
            //Debug.Log("barco_sushi.cs//barco ainda não chegou na sua posição inicial");
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 1, gameObject.transform.position.z);
            yield return new WaitForSeconds(.100f);
        }

        gameObject.transform.position = posicaoInicialBarco;
        //por fim, fazer o barco e seu texto voltar ao canvas origital dele
        GameObject canvasLayerDoBarco = GameObject.Find("Canvas");
        //Debug.Log("barco_sushi.cs//canvas encontrado=" + canvasLayerDoBarco.name);
        transform.parent = canvasLayerDoBarco.transform;
        objetoTextoAcompanhaBarco.transform.parent = canvasLayerDoBarco.transform;
        GetComponent<Renderer>().sortingOrder = -1;
        //e tornar o barco jogável de nv
        gameObject.GetComponent<EdgeCollider2D>().enabled = true;
        //e tornar toggle sushis transparentes interativo de nv
        string nomeToggleSushisVisiveis = "botao_visivel_sushi_boat_" + numeroUmObjetoBarco;
        Toggle toggleBotaoTornarSushisInvisiveis = GameObject.Find(nomeToggleSushisVisiveis).GetComponent<Toggle>();
        toggleBotaoTornarSushisInvisiveis.interactable = true;


        //só uma coisinha: checar se o barco está com frase de forma verbal ou de partícula na hora certa
        frase_barco fraseNovaDoBarco = objetoTextoAcompanhaBarco.GetComponent<frase_barco>();
        banco_de_dados.fraseDaBarcaca dadosDaFraseAssociada = fraseNovaDoBarco.getFraseAssociada();
        string ehParticulaOuFormaVerbal = dadosDaFraseAssociada.getEhParticulaOuFormaVerbal();
        if (script_cronometro.current_timer < 840 && ehParticulaOuFormaVerbal.Contains("partícula") == false)
        {
            geraNovaFraseBarco.mudarTextoDaBarcaca(numeroEmIntDoObjetoBarco);
        }
        if(script_cronometro.current_timer >= 840 && ehParticulaOuFormaVerbal.Contains("forma verbal") == false)
        {
            geraNovaFraseBarco.mudarTextoDaBarcaca(numeroEmIntDoObjetoBarco);
        }


        barcoSumindo = false;

    }





    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.name.StartsWith("sushi_") == true)
        {
            bool jahSomeiSushisNoBarco = false;
            if(quantosSushisEmCimaDoBarco < quantosSushisNecessariosPraFrase)
            {
                //objeto que colidiu é um novo sushi e dá pra inserir um novo sushi no barco!!
                
                Debug.Log("barco_sushi.cs// novo sushi entrou no barco! -- trigger enter");
                sushi_script sushiColidiuComBarcaca = col.gameObject.GetComponent<sushi_script>();
                sushiColidiuComBarcaca.setSushiEmCimaDeBarcoOuCumbuca(true);
                sushiColidiuComBarcaca.setSushiEmCimaDeBarco(true, this);
                //inserir sushi na list de sushis no barco
                GameObject novoSushiInserir = col.gameObject;
                sushisEmCimaDeBarco.AddLast(novoSushiInserir);
                foreach (var sushiNoBarco in sushisEmCimaDeBarco)
                {
                    atualizarPosicaoSushiNoBarco(sushiNoBarco);
                }
            }

            if (Input.GetMouseButton(0) == true)
            {
                //Debug.Log("barco_sushi.cs//botão do mouse estah sendo pressionado");
                //botão do mouse está sendo pressionado

                if (quantosSushisEmCimaDoBarco < quantosSushisNecessariosPraFrase)
                {
                    //Debug.Log("barco_sushi.cs//tem menos sushis");
                    quantosSushisEmCimaDoBarco = quantosSushisEmCimaDoBarco + 1;
                    //Debug.Log("barco_sushi.cs//quantosSushisTEmNoBarco=" + quantosSushisEmCimaDoBarco);
                    //Debug.Log("barco_sushi.cs//quantosSushisNecessariosPraFrase=" + quantosSushisNecessariosPraFrase);
                    jahSomeiSushisNoBarco = true;
                    if(quantosSushisEmCimaDoBarco == quantosSushisNecessariosPraFrase)
                    {
                        //Debug.Log("barco_sushi.cs//inspetorVisivel=" + inspetor_script.inspetorVisivel);
                        if (inspetor_script.inspetorVisivel == false || (inspetor_script.inspetorVisivel == true && inspetor_script.emQualBarcacaInspetorEstah != numBarcoSushi))
                        {
                            //Debug.Log("barco_sushi.cs//ativar botão!");
                            this.botaoOkAssociadoAoBarco.ativarBotao();
                        }
                        
                    }
                    GetComponent<SpriteRenderer>().sprite = barcoHighlight;
                }
                else
                {
                    //Debug.Log("barco_sushi.cs//tem mais  sushis");
                    GetComponent<SpriteRenderer>().sprite = barcoHighlightBlock;
                }
                

            }
            if(sushiEstavaNoBarco(col.gameObject) && jahSomeiSushisNoBarco == false)
            {
                quantosSushisEmCimaDoBarco = quantosSushisEmCimaDoBarco + 1;
                if (quantosSushisEmCimaDoBarco == quantosSushisNecessariosPraFrase)
                {

                    this.botaoOkAssociadoAoBarco.ativarBotao();
                }
            }
            if(sushisEstaoTransparentes == true)
            {
                //de qualquer forma, setar o aplha desse sushi pra ser invisível se é para os sushis ficarem transparentes
                string nomeObjeto = col.gameObject.transform.name;
                string[] nome_splitado = nomeObjeto.Split('_');
                string numeroDoSushi = nome_splitado[nome_splitado.Length - 1];
                string nomeTextoEmCimaSushi = "texto_sushi_" + numeroDoSushi;
                string nomeTextoOrdemSushi = "ordem_sushi_" + numeroDoSushi;
                GameObject textoEmCimaSushi = GameObject.Find(nomeTextoEmCimaSushi);
                GameObject textoOrdemSushi = GameObject.Find(nomeTextoOrdemSushi);
                //Debug.Log("barco_sushi.cs//sushi name em cima do barco=" + sushiEmCimaDoBarco.name);
                col.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.1f);
                textoEmCimaSushi.GetComponent<CanvasRenderer>().SetAlpha(0.1f);
                textoOrdemSushi.GetComponent<CanvasRenderer>().SetAlpha(0.1f);
            }
            //Debug.Log("barco_sushi.cs//quantos sushis em cima do barco=" + quantosSushisEmCimaDoBarco);
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        //print("Are you even colliding my friend?");
        if (col.name.StartsWith("sushi_") == true && sushiEstavaNoBarco(col.gameObject) == true)
        {
            //objeto que colidiu é um sushi!
            if(quantosSushisEmCimaDoBarco == quantosSushisNecessariosPraFrase)
            {
                this.botaoOkAssociadoAoBarco.desativarBotao();
            }
            quantosSushisEmCimaDoBarco = quantosSushisEmCimaDoBarco - 1;
            
            //Debug.Log("barco_sushi.cs// novo sushi saiu no barco!");
            sushi_script sushiColidiuComBarcaca = col.gameObject.GetComponent<sushi_script>();
            sushiColidiuComBarcaca.setSushiEmCimaDeBarcoOuCumbuca(false);
            sushiColidiuComBarcaca.setSushiEmCimaDeBarco(false, this);
            //tem de remover o sushi da lista de sushis no barco
            GameObject sushiRemover = col.gameObject;
            sushisEmCimaDeBarco.Remove(sushiRemover);
            foreach(var sushiNoBarco in sushisEmCimaDeBarco)
            {
                atualizarPosicaoSushiNoBarco(sushiNoBarco);
            }

            //de qualquer forma, setar o aplha desse sushi pra ser visível
            string nomeObjeto = col.gameObject.transform.name;
            string[] nome_splitado = nomeObjeto.Split('_');
            string numeroDoSushi = nome_splitado[nome_splitado.Length - 1];
            string nomeTextoEmCimaSushi = "texto_sushi_" + numeroDoSushi;
            string nomeTextoOrdemSushi = "ordem_sushi_" + numeroDoSushi;
            GameObject textoEmCimaSushi = GameObject.Find(nomeTextoEmCimaSushi);
            GameObject textoOrdemSushi = GameObject.Find(nomeTextoOrdemSushi);
            //Debug.Log("barco_sushi.cs//sushi name em cima do barco=" + sushiEmCimaDoBarco.name);
            col.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1.0f);
            textoEmCimaSushi.GetComponent<CanvasRenderer>().SetAlpha(1.0f);
            textoOrdemSushi.GetComponent<CanvasRenderer>().SetAlpha(1.0f);



        }

        GetComponent<SpriteRenderer>().sprite = barcoNormal;
    }

    private bool sushiEstavaNoBarco(GameObject sushi)
    {
        bool sushiEstahNoBarco = false;
        foreach(var sushiNoBarco in sushisEmCimaDeBarco)
        {
            if(sushiNoBarco == sushi)
            {
                sushiEstahNoBarco = true;
                break;
            }
        }
        return sushiEstahNoBarco;
    }

    public void atualizarPosicaoSushiNoBarco(GameObject sushi)
    {
        int posicaoSushiNoBarco = 1;
        foreach (var sushiNoBarco in sushisEmCimaDeBarco)
        {
            if (sushiNoBarco != sushi && sushiNoBarco.transform.position.x < sushi.transform.position.x)
            {
                posicaoSushiNoBarco = posicaoSushiNoBarco + 1;
            }
        }
        //só faltou setar a posicao do sushi no barco
        sushi.GetComponent<sushi_script>().setOrdemSushiNoBarco(posicaoSushiNoBarco);
    }

    public void atualizarPosicaoTodosOsSushisNoBarco()
    {
        foreach(var sushiNoBarco in sushisEmCimaDeBarco)
        {
            atualizarPosicaoSushiNoBarco(sushiNoBarco);
        }

    }

    void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().sprite = barcoNormal;

    }

    public void esvaziarBarcacaESomarPontos()
    {
        //primeiro, achar o objeto texto da barcaça pra pegar o texto dela
        string nomeObjeto = gameObject.transform.name;
        string[] nome_splitado = nomeObjeto.Split('_');
        string numeroDaFraseBarco = nome_splitado[nome_splitado.Length - 1];
        string nomeFraseBarcoAssociado = "frase_sushi_boat_" + numeroDaFraseBarco;
        GameObject objetoFraseBarco = GameObject.Find(nomeFraseBarcoAssociado);
        frase_barco fraseBarcoAssociado = objetoFraseBarco.GetComponent<frase_barco>();
        banco_de_dados.fraseDaBarcaca dadosDaFraseBarcaca = fraseBarcoAssociado.getFraseAssociada();
        string respostaComPontoEVirgula = dadosDaFraseBarcaca.getResposta();
        string[] respostasSeparadas = respostaComPontoEVirgula.Split(',');
        //pegar os sushis em cima do barco e ordená-los em relação às posições deles no barco
        GameObject[] sushisOrdenados = new GameObject[quantosSushisEmCimaDoBarco];
        foreach(var sushiNoBarco in sushisEmCimaDeBarco)
        {
            int posicaoSushiNoArray = 0;
            foreach (var sushiComparado in sushisEmCimaDeBarco)
            {
                if (sushiNoBarco != sushiComparado && sushiComparado.transform.position.x < sushiNoBarco.transform.position.x)
                {
                    posicaoSushiNoArray = posicaoSushiNoArray + 1;
                }
            }
            sushisOrdenados[posicaoSushiNoArray] = sushiNoBarco;
        }
        //fazer os sushis aparecerem se eles estão semitransparentes
        string nomeToggleSushisVisiveis = "botao_visivel_sushi_boat_" + numeroDaFraseBarco;
        Toggle toggleBotaoTornarSushisInvisiveis = GameObject.Find(nomeToggleSushisVisiveis).GetComponent<Toggle>();
        if (sushisEstaoTransparentes == true)
        {
            //Debug.Log("barco_sushi.cs//sushis estão transparentes!");
            toggleBotaoTornarSushisInvisiveis.isOn = false;
        }
        toggleBotaoTornarSushisInvisiveis.interactable = false;

        //Debug.Log("barco_sushi.cs//tamanho array sushisOrdenados=" + sushisOrdenados.Length);
        //temos os sushis e as respostas. agora, vamos comparar quais respostas estão corretas
        double quantoDeDinheiroUsuarioGanhouAcertandoSushis = 0.0d;
        for(int i = 0; i < respostasSeparadas.Length; i++)
        {
            string umaRepostaCorreta = respostasSeparadas[i];
            GameObject umObjetoSushi = sushisOrdenados[i];
            string nomeUmObjetoSushi = umObjetoSushi.name;
            string[] nomeSushi_splitado = nomeUmObjetoSushi.Split('_');
            string numeroSushi = nomeSushi_splitado[nomeSushi_splitado.Length - 1];
            string nomeObjetoTextoSushi = "texto_sushi_" + numeroSushi;
            GameObject objetoTextoSushi = GameObject.Find(nomeObjetoTextoSushi);
            string umaRespostaDoUsuario = objetoTextoSushi.GetComponent<Text>().text;
            //Debug.Log("barco_sushi.cs//resposta correta=" + umaRepostaCorreta);
            //Debug.Log("barco_sushi.cs//resposta do usuario=" + umaRespostaDoUsuario.Replace("\\n", ""));
            bool usuarioAcertouResposta = false;
            if (umaRepostaCorreta.Equals(umaRespostaDoUsuario) == true || ( umaRepostaCorreta.Contains("/") == true  && umaRepostaCorreta.Contains(umaRespostaDoUsuario) == true)  )
            {
                usuarioAcertouResposta = true;
                //Debug.Log("barco_sushi.cs//resposta correta!");
                quantoDeDinheiroUsuarioGanhouAcertandoSushis = quantoDeDinheiroUsuarioGanhouAcertandoSushis + 2.50d;//sushi = 2.50 reais
            }
            //fazer sushis ddesaparecerem e a frase do barco ter as partículas no lugar do 1,2 e 3(com coor verde se usuário acertou e vermelho se usuário errou)
            umObjetoSushi.GetComponent<Renderer>().enabled = false;
            objetoTextoSushi.GetComponent<Text>().enabled = false;
            string nomeTextoOrdemSushi = "ordem_sushi_" + numeroSushi;
            GameObject objetoTextoOrdemSushi = GameObject.Find(nomeTextoOrdemSushi);
            objetoTextoOrdemSushi.GetComponent<Text>().enabled = false;
            //mudar frase do barco para ter as partículas coloridas
            int ordemDoSushi = i + 1;
            string textoParticula = "<color=#";
            if (usuarioAcertouResposta == true)
            {
                textoParticula = textoParticula + "00D100FF";
            }
            else
            {
                textoParticula = textoParticula + "D50005FF";
            }
            textoParticula = textoParticula + ">" + umaRespostaDoUsuario + "</color>";
            if(usuarioAcertouResposta == false)
            {
                textoParticula = textoParticula + "「<color=#00D100FF>" + umaRepostaCorreta+"</color>」";
            }
            //Debug.Log("barco_sushi.cs//texto_particula = " + textoParticula);
            GameObject objetoTextoSushiBoat = GameObject.Find("frase_sushi_boat_" + numeroDaFraseBarco);
            Text textoSushiBoat = objetoTextoSushiBoat.GetComponent<Text>();
            string stringTextoBarco = textoSushiBoat.text;
            string novoTextoBarco;
            if(ordemDoSushi == 1)
            {
                novoTextoBarco = stringTextoBarco.Replace("<color=##ff0000ff>一</color>", textoParticula);
            }
            else if (ordemDoSushi == 2)
            {
                novoTextoBarco = stringTextoBarco.Replace("<color=##ff0000ff>二</color>", textoParticula);
            }
            else if (ordemDoSushi == 3)
            {
                novoTextoBarco = stringTextoBarco.Replace("<color=##ff0000ff>三</color>", textoParticula);
            }
            else 
            {
                novoTextoBarco = stringTextoBarco.Replace("<color=##ff0000ff>四</color>", textoParticula);
            }

            //Debug.Log("barco_sushi.cs//novo texto barco=" + novoTextoBarco);
            textoSushiBoat.text = novoTextoBarco;
        }
        //incrementar o quanto o usuario ganhou nos sushis
        GameObject objetoDinheiroDoJogador = GameObject.Find("texto_money_sushi");
        money_sushi dinheiroDoJogador = objetoDinheiroDoJogador.GetComponent<money_sushi>();
        if(script_cronometro.current_timer >= 840)
        {
            quantoDeDinheiroUsuarioGanhouAcertandoSushis = quantoDeDinheiroUsuarioGanhouAcertandoSushis * 2;
        }
        dinheiroDoJogador.aumentarDinheiro(quantoDeDinheiroUsuarioGanhouAcertandoSushis);
       



        //impossibilitar o usuário de mexer o barco e os sushis no barco
        gameObject.GetComponent<EdgeCollider2D>().enabled = false;
        for(int u = 0; u < sushisOrdenados.Length; u++)
        {
            GameObject umSushi = sushisOrdenados[u];
            umSushi.GetComponent<PolygonCollider2D>().enabled = false;
        }

        //e desativar o botão
        botaoOkAssociadoAoBarco.desativarBotao();

        //vamos fazer o jogo esperar um pouquinho e levantar o barco
        fazerSumirBarco = true;

    }

    public void toggleSushisTransparentesOuNao()
    {
        
        if (sushisEstaoTransparentes == true)
        {
            sushisEstaoTransparentes = false;
            //fazer os barcos em cima do sushi meio opacos
            foreach(var sushiEmCimaDoBarco in sushisEmCimaDeBarco)
            {
                string nomeObjeto = sushiEmCimaDoBarco.transform.name;
                string[] nome_splitado = nomeObjeto.Split('_');
                string numeroDoSushi = nome_splitado[nome_splitado.Length - 1];
                string nomeTextoEmCimaSushi = "texto_sushi_" + numeroDoSushi;
                string nomeTextoOrdemSushi = "ordem_sushi_" + numeroDoSushi;
                GameObject textoEmCimaSushi = GameObject.Find(nomeTextoEmCimaSushi);
                GameObject textoOrdemSushi = GameObject.Find(nomeTextoOrdemSushi);
                //Debug.Log("barco_sushi.cs//sushi name em cima do barco=" + sushiEmCimaDoBarco.name);
                sushiEmCimaDoBarco.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1.0f);
                textoEmCimaSushi.GetComponent<CanvasRenderer>().SetAlpha(1.0f);
                textoOrdemSushi.GetComponent<CanvasRenderer>().SetAlpha(1.0f);
                //Debug.Log("barco_sushi.cs//agora os sushisficaramVisiveis!");
            }


        }
        else
        {
            sushisEstaoTransparentes = true;
            foreach (var sushiEmCimaDoBarco in sushisEmCimaDeBarco)
            {
                string nomeObjeto = sushiEmCimaDoBarco.transform.name;
                string[] nome_splitado = nomeObjeto.Split('_');
                string numeroDoSushi = nome_splitado[nome_splitado.Length - 1];
                string nomeTextoEmCimaSushi = "texto_sushi_" + numeroDoSushi;
                string nomeTextoOrdemSushi = "ordem_sushi_" + numeroDoSushi;
                GameObject textoEmCimaSushi = GameObject.Find(nomeTextoEmCimaSushi);
                GameObject textoOrdemSushi = GameObject.Find(nomeTextoOrdemSushi);
                //Debug.Log("barco_sushi.cs//sushi name em cima do barco=" + sushiEmCimaDoBarco.name);
                sushiEmCimaDoBarco.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.1f);
                textoEmCimaSushi.GetComponent<CanvasRenderer>().SetAlpha(0.1f);
                textoOrdemSushi.GetComponent<CanvasRenderer>().SetAlpha(0.1f);
                //Debug.Log("barco_sushi.cs//agora os sushisficaram invisiveis!");
            }
        }
    }

    public void verificarSeDeveAparecerShoyuOuNao()
    {
        if (quantosSushisEmCimaDoBarco == quantosSushisNecessariosPraFrase)
        {
            //Debug.Log("barco_sushi.cs//inspetorVisivel=" + inspetor_script.inspetorVisivel);
            if (inspetor_script.inspetorVisivel == false || (inspetor_script.inspetorVisivel == true && inspetor_script.emQualBarcacaInspetorEstah != numBarcoSushi))
            {
                //Debug.Log("barco_sushi.cs//ativar botão!");
                this.botaoOkAssociadoAoBarco.ativarBotao();
            }

        }
    }



    




}
