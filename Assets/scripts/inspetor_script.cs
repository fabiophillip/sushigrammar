﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class inspetor_script : MonoBehaviour
{

    public Sprite inspetorNormal;
    public Sprite inspetorHighlight;
    public static bool inspetorVisivel;
    public static int emQualBarcacaInspetorEstah;
    private int ultimaBarcacaInspetorFicou;
    private Vector3 posicaoInicialInspetor;
    private bool ehHoraDoMinijogoInspetor;
    private volatile bool tempoMinijogoAcabou;
    private int tempoMinijogo;
    private banco_de_dados.palavraVocabulario palavraEscolhidaProMinijogo;




    // Use this for initialization
    void Start()
    {
        inspetorVisivel = false;
        emQualBarcacaInspetorEstah = 0;
        posicaoInicialInspetor = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
       
       
        
    }

    
   



    IEnumerator passarTempoMinijogo()
    {
        while (tempoMinijogoAcabou == false)
        {
            yield return new WaitForSeconds(1);
            tempoMinijogo = tempoMinijogo - 1;
            GameObject.Find("tempo_restante_inspetor").GetComponent<Text>().text = "" + tempoMinijogo;
            if (tempoMinijogo <= 0)
            {
                tempoMinijogoAcabou = true;
                StartCoroutine(mostrarResultsMiniJogo("", null));
            }
        }
    }

    public void usuarioSelecionouAlternativa(string respostaDoUsuario, GameObject objetoUsuarioClicou)
    {
        StopCoroutine(passarTempoMinijogo());
        StartCoroutine(mostrarResultsMiniJogo(respostaDoUsuario, objetoUsuarioClicou));
    }


    IEnumerator mostrarResultsMiniJogo(string respostaDoUsuario, GameObject objetoUsuarioClicou)
    {
        if(ehHoraDoMinijogoInspetor == true)
        {
            //primeiro, só deixar visível o background das alternativas, as alternativas 
            GameObject.Find("inspetor_1").GetComponent<Renderer>().enabled = false;
            GameObject.Find("speech_bubble_inspetor").GetComponent<Renderer>().enabled = false;
            GameObject.Find("background_relogio_inspetor").GetComponent<Renderer>().enabled = false;
            GameObject.Find("pergunta_inspetor").GetComponent<Text>().enabled = false;
            GameObject.Find("tempo_restante_inspetor").GetComponent<Text>().enabled = false;
            //mudar a cor do objeto selecionado para dar feedback
            objetoUsuarioClicou.GetComponent<Text>().text = "<color=#FF976EFF>> " + respostaDoUsuario + "</color>";

            //agora, fazer as imagens do inspetor pensando aparecerem uma por vez a cada segundo
            GameObject.Find("inspetor_barcaca_pensando_1").GetComponent<Renderer>().enabled = true;
            yield return new WaitForSeconds(1);
            GameObject.Find("inspetor_barcaca_pensando_2").GetComponent<Renderer>().enabled = true;
            yield return new WaitForSeconds(1);
            GameObject.Find("inspetor_barcaca_pensando_3").GetComponent<Renderer>().enabled = true;
            yield return new WaitForSeconds(2);
            //fazer desaparecer os inspetores pensando...
            GameObject.Find("inspetor_barcaca_pensando_1").GetComponent<Renderer>().enabled = false;
            GameObject.Find("inspetor_barcaca_pensando_2").GetComponent<Renderer>().enabled = false;
            GameObject.Find("inspetor_barcaca_pensando_3").GetComponent<Renderer>().enabled = false;
            //e fazer aparecer o balão e o inspetor falando no balão
            GameObject.Find("inspetor_1").GetComponent<Renderer>().enabled = true;
            GameObject.Find("speech_bubble_inspetor").GetComponent<Renderer>().enabled = true;
            //o usuario acertou ou não? dependendo disso, a frase do inspetor deveria mudar

            string fraseNovaInspetor;
            bool usuarioGanhou;
            if (respostaDoUsuario == palavraEscolhidaProMinijogo.getTraducao())
            {
                usuarioGanhou = true;
                //usuário acertou!
                fraseNovaInspetor = "せいかいです。\n おつかれさまでした。";
                GameObject.Find("bonus_money").GetComponent<Text>().text = "Ganhou \n + R$10,00!";
                GameObject.Find("bonus_money").GetComponent<Text>().enabled = true;
                objetoUsuarioClicou.GetComponent<Text>().text = "<color=#00D100FF>> " + respostaDoUsuario + "</color>";

            }
            else
            {
                usuarioGanhou = false;
                fraseNovaInspetor = "あのー　ちがうですが　\n　あとで　がんばってくださいね。";
                GameObject.Find("bonus_money").GetComponent<Text>().text = "Não foi\n dessa vez...";
                GameObject.Find("bonus_money").GetComponent<Text>().enabled = true;
                if (objetoUsuarioClicou != null)
                {
                    objetoUsuarioClicou.GetComponent<Text>().text = "<color=#D50005FF>> " + respostaDoUsuario + "</color>";
                }
                for (int i = 1; i < 5; i++)
                {
                    GameObject objetoAlternativa = GameObject.Find("alterativa_inspetor_" + i);
                    Text textoAlternativa = objetoAlternativa.GetComponent<Text>();
                    string stringAlternativa = textoAlternativa.text.Replace("> ", "");
                    if (stringAlternativa == palavraEscolhidaProMinijogo.getTraducao())
                    {
                        textoAlternativa.text = "<color=#00D100FF>> " + stringAlternativa + "</color>";
                    }
                }
            }

            GameObject.Find("pergunta_inspetor").GetComponent<Text>().text = fraseNovaInspetor;
            GameObject.Find("pergunta_inspetor").GetComponent<Text>().enabled = true;

            yield return new WaitForSeconds(7);

            terminarMinijogo(usuarioGanhou);

        }
        



    }

    public void comecarOMinijogo()
    {
        if(inspetorVisivel == true)
        {
            //o mouse clicou no inspetor
            Debug.Log("inspetor_script.cs//mouse clicou no inspetor");
            //primeiro, pegar uma palavra pro minijogo
            GameObject objetoBancoDeDados = GameObject.Find("banco_de_dados");
            banco_de_dados bancoDados = objetoBancoDeDados.GetComponent<banco_de_dados>();
            int numeroBarcaca = emQualBarcacaInspetorEstah;
            palavraEscolhidaProMinijogo = bancoDados.pegarPalavraVocabularioAssociada(numeroBarcaca);

            inspetorVisivel = false;
            this.transform.position = this.posicaoInicialInspetor;
            emQualBarcacaInspetorEstah = 0;
            Debug.Log("inspetor_script.cs//eh_hora_minijogo_inspetor");

            //primeiro, parar o tempo
            script_cronometro.tempo_ativo = false;


            GameObject backgroundMinijogoInspetor = GameObject.Find("background_minijogo_inspetor");
            backgroundMinijogoInspetor.GetComponent<Renderer>().enabled = true;
            Renderer[] renderersSpritesMInijogoInspetor = backgroundMinijogoInspetor.transform.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderersSpritesMInijogoInspetor.Length; i++)
            {
                renderersSpritesMInijogoInspetor[i].enabled = true;
            }

            GameObject objetoTextoTempo = GameObject.Find("tempo_restante_inspetor");
            objetoTextoTempo.GetComponent<Text>().enabled = true;

            Text[] renderersTextosMinijogoInspetor = objetoTextoTempo.transform.GetComponentsInChildren<Text>();
            for (int j = 0; j < renderersTextosMinijogoInspetor.Length; j++)
            {
                renderersTextosMinijogoInspetor[j].enabled = true;
            }

            //setar o texto da palavra que o jogador vai treinar e das alternativas à tradução da palavra
            Text textoDoInspetor = GameObject.Find("pergunta_inspetor").GetComponent<Text>();
            textoDoInspetor.text = "「" + palavraEscolhidaProMinijogo.getPalavra() + "」は　ポルトガルご　で\n　 なんですか。";

            List<string> alternativasDoJogo = new List<string>();
            alternativasDoJogo.Add(palavraEscolhidaProMinijogo.getTraducao());
            alternativasDoJogo.Add(palavraEscolhidaProMinijogo.getPegadinha());
            string[] outrasAlternativas = palavraEscolhidaProMinijogo.getTraducoesErradasAlternativasParaJogo();
            alternativasDoJogo.Add(outrasAlternativas[0]);
            alternativasDoJogo.Add(outrasAlternativas[1]);
            List<string> listaEmbaralhada = embaralharLista(alternativasDoJogo);
            for (int i = 0; i < listaEmbaralhada.Count; i++)
            {
                string umaAlternativa = listaEmbaralhada[i];
                int numeroAlternativa = i + 1;
                GameObject objetoAlternativa = GameObject.Find("alterativa_inspetor_" + numeroAlternativa);
                objetoAlternativa.GetComponent<BoxCollider2D>().enabled = true;
                objetoAlternativa.GetComponent<alternativa_minijogo_inspetor>().enabled = true;
                Text textoAlternativa = objetoAlternativa.GetComponent<Text>();
                textoAlternativa.text = "> " + umaAlternativa;
            }


            ehHoraDoMinijogoInspetor = true;
            tempoMinijogoAcabou = false;
            tempoMinijogo = 30;
            GameObject.Find("tempo_restante_inspetor").GetComponent<Text>().text = "" + tempoMinijogo;
            //falta iniciar cronometro e começar a partida

            StartCoroutine(passarTempoMinijogo());
        }

    }


    public void terminarMinijogo(bool usuarioGanhou)
    {
        script_cronometro.tempo_ativo = true;


        GameObject backgroundMinijogoInspetor = GameObject.Find("background_minijogo_inspetor");
        backgroundMinijogoInspetor.GetComponent<Renderer>().enabled = false;
        Renderer[] renderersSpritesMInijogoInspetor = backgroundMinijogoInspetor.transform.GetComponentsInChildren<Renderer>();
        for (int i = 0; i < renderersSpritesMInijogoInspetor.Length; i++)
        {
            renderersSpritesMInijogoInspetor[i].enabled = false;
        }
        GameObject.Find("bonus_money").GetComponent<Text>().enabled = false;
        

        GameObject objetoTextoTempo = GameObject.Find("tempo_restante_inspetor");
        objetoTextoTempo.GetComponent<Text>().enabled = false;

        Text[] renderersTextosMinijogoInspetor = objetoTextoTempo.transform.GetComponentsInChildren<Text>();
        for (int j = 0; j < renderersTextosMinijogoInspetor.Length; j++)
        {
            renderersTextosMinijogoInspetor[j].enabled = false;
        }
        //desabilitar os scripts das alternativas do minijogo
        for(int i = 1; i < 5; i++)
        { 
            GameObject objetoAlternativa = GameObject.Find("alterativa_inspetor_" + i);
            objetoAlternativa.GetComponent<alternativa_minijogo_inspetor>().enabled = false;
            objetoAlternativa.GetComponent<BoxCollider2D>().enabled = false;
        }

        ehHoraDoMinijogoInspetor = false;

        //falta adicionar o money do jogador
        if(usuarioGanhou == true)
        {
            GameObject objetoMoney = GameObject.Find("texto_money_sushi");
            money_sushi dinheiro = objetoMoney.GetComponent<money_sushi>();
            dinheiro.aumentarDinheiro(10.0f); 
        }

        //se o inspetor estava num barco onde já foram colocados todos os sushis em cima, fazer aparecer o shoyu
        string nomeBarcoInspetorFicouPorUltimo = "sushi_boat_" + ultimaBarcacaInspetorFicou;
        barco_sushi barcoSushiFicou = GameObject.Find(nomeBarcoInspetorFicouPorUltimo).GetComponent<barco_sushi>();
        barcoSushiFicou.verificarSeDeveAparecerShoyuOuNao();
        

    }

    public void ativarInspetor()
    {

        int idQualBarcacaInspetorVai = 0;
        bool achouBarcacaNãoEstahSeMovendoOuEstahCheia = false;
        while(temBarcacaDisponivelParaInspetor() == true && achouBarcacaNãoEstahSeMovendoOuEstahCheia == false)
        {
            idQualBarcacaInspetorVai  = UnityEngine.Random.Range(1, 4);
            string nomeBarcacaInspetorVai = "sushi_boat_" + idQualBarcacaInspetorVai;
            emQualBarcacaInspetorEstah = idQualBarcacaInspetorVai;
            ultimaBarcacaInspetorFicou = idQualBarcacaInspetorVai;
            GameObject objetoBarcoInspetorEstah = GameObject.Find(nomeBarcacaInspetorVai);
            barco_sushi barco = objetoBarcoInspetorEstah.GetComponent<barco_sushi>();
            if(barco.getBarcoSumindo() == false && barco.getBarcoCheio() == false)
            {
                achouBarcacaNãoEstahSeMovendoOuEstahCheia = true;
                GameObject objetoShoyuDaBarcacaAssociada = GameObject.Find("checked_button_boat_" + idQualBarcacaInspetorVai);
                this.transform.position = objetoShoyuDaBarcacaAssociada.transform.position;
                GetComponent<Image>().enabled = true;
                inspetorVisivel = true;
            }
        }
        
        
    }

    private bool temBarcacaDisponivelParaInspetor()
    {
        bool temBarcacaDisponivel = false;
        for(int i = 1; i < 4; i++)
        {
            string nomeBarcacaInspetorVai = "sushi_boat_" + i;
            GameObject objetoBarcoInspetorEstah = GameObject.Find(nomeBarcacaInspetorVai); 
            barco_sushi barco = objetoBarcoInspetorEstah.GetComponent<barco_sushi>();
            if (barco.getBarcoSumindo() == false && barco.getBarcoCheio() == false)
            {
                temBarcacaDisponivel = true;
            }
        }
        return temBarcacaDisponivel;
    }


    public List<string> embaralharLista(List<string> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n);
            string value = list[k];
            list[k] = list[n];
            list[n] = value;
        }

        return list;
    }
}
