﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class objetivo_do_dia : MonoBehaviour {

    private int valorMetaDoDia = 100;
    private string metaDoDia = "dinheiro";

	// Use this for initialization
	void Start () {
        Text textoMemoObjetivoDoDia = GetComponent<Text>();
        if(metaDoDia == "dinheiro")
        {
            string stringTexto = "Objetivo do dia:\n ganhar R$ " + valorMetaDoDia;
            textoMemoObjetivoDoDia.text = stringTexto;
        }
	
	}

    public string getMetadoDia()
    {
        return metaDoDia;
    }
    public void setValorMetaDoDia(int novoValor)
    {
        this.valorMetaDoDia = novoValor;
    }
    public void setMetaDoDia(string novoValor)
    {
        this.metaDoDia = novoValor;
    }

    public int getValorMetaDoDia()
    {
        return valorMetaDoDia;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
