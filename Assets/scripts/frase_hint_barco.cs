﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class frase_hint_barco : MonoBehaviour {

    private banco_de_dados.fraseDaBarcaca fraseAssociada;
    GameObject barcoSeguir;

    // Use this for initialization
    void Start()
    {
        string nomeObjeto = gameObject.transform.name;
        string[] nome_splitado = nomeObjeto.Split('_');
        string numeroDaFraseBarco = nome_splitado[nome_splitado.Length - 1];
        string nomeSushiBoatAssociado = "sushi_boat_" + numeroDaFraseBarco;
        //Debug.Log("frase_barco.cs///barco_sushi_associado = " + nomeSushiBoatAssociado);
        barcoSeguir = GameObject.Find(nomeSushiBoatAssociado);
        

    }

    public banco_de_dados.fraseDaBarcaca getFraseAssociada()
    {
        return this.fraseAssociada;
    }

    public void setFraseAssociada(banco_de_dados.fraseDaBarcaca novoValor)
    {
        fraseAssociada = novoValor;
        //falta mudar o próprio texto da frase do barco
        string textoDaHint = fraseAssociada.getDicaFormaVerbal();
        //Debug.Log("frase_barco.cs//texto do barco " + textoDoBarco);
        

        GetComponent<Text>().text = textoDaHint;

       

    }

    // Update is called once per frame
    void Update()
    {
       


    }
}
