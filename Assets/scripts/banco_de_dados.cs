﻿using UnityEngine;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using System.Data;
using System;
using System.IO;
using System.Text.RegularExpressions;

public class banco_de_dados : MonoBehaviour {

    public static int [] niveis_usar_no_jogo = new int[] {1};
    LinkedList<particulaOuFormaVerbal> particulasUsadasNaJogatina;
    LinkedList<particulaOuFormaVerbal> formasVerbaisUsadasNaJogatina;
    LinkedList<fraseDaBarcaca> frasesTreinamParticulasUsarNasBarcacas;
    LinkedList<fraseDaBarcaca> frasesTreinamFormasVerbaisUsarNasBarcacas;
    LinkedList<palavraVocabulario> palavrasTreinadasNoJogo;
    private string [] frasesJahNasBarcacas;//as frases que já estão na barcaça 1, 2 e 3, nessa ordem
    // Use this for initialization
    void Start () {

        //criar lista com as partículas/formas verbais usadas no jogo e frases
        particulasUsadasNaJogatina = new LinkedList<particulaOuFormaVerbal>();
        formasVerbaisUsadasNaJogatina = new LinkedList<particulaOuFormaVerbal>();
        frasesTreinamParticulasUsarNasBarcacas = new LinkedList<fraseDaBarcaca>();
        frasesTreinamFormasVerbaisUsarNasBarcacas = new LinkedList<fraseDaBarcaca>();
        palavrasTreinadasNoJogo = new LinkedList<palavraVocabulario>();
        frasesJahNasBarcacas = new string[3];
        TextAsset textAssetParticulasEFormasVerbais = Resources.Load("estruturas_gramatica_jlpt_n5") as TextAsset;
        this.parseTextAssetParticulaOuFormaVerbal(textAssetParticulasEFormasVerbais);

        //segundo, pegar as frases a serem treinadas no jogo
        TextAsset textAssetFrases = Resources.Load("perguntas_para_jogo_sushi_grammar_formatado") as TextAsset;
        this.parseTextAssetFrasesDoJogo(textAssetFrases);

        //terceiro, pegar o vocabulário a ser treinado no jogo
        TextAsset textAssetVocabulario = Resources.Load("vocabulario_sushi_grammar") as TextAsset;
        this.parseTextAssetVocabularioTreinado(textAssetVocabulario);


        //extrai todas as partículas usadas no jogo para o banco. falta setar elas nos textos do sushi
        //onStart(): no começo, vamos setar o texto dos sushis iniciais. há 6 no começo
        for (int i = 1; i < 7; i++)
        {
            mudarTextoDoSushi(i);
        }
        //também vamos setar o texto nas barcaças. há sempre 3.
        for (int i = 1; i < 4; i++)
        {
            mudarTextoDaBarcaca(i);
        }

    }



    private void parseTextAssetParticulaOuFormaVerbal(TextAsset ft)
    {

        string fs = ft.text;
        string[] fLines = Regex.Split(fs, "\n|\r|\r\n");

        for (int i = 0; i < fLines.Length; i++)
        {
            //Debug.Log("banco_de_dados.cs//linha do csv particulas=" + fLines[i]);
            string valueLine = fLines[i];
            if(valueLine.Length > 0)
            {
                string[] values = Regex.Split(valueLine, ";");

                string particula_ou_forma_verbal = values[1];
                string estrutura_gramatical = values[2];
                int nivel_no_jogo;
                if (values[3].Length > 0)
                {
                    nivel_no_jogo = int.Parse(values[3]);
                }
                else
                {
                    nivel_no_jogo = -1;
                }

                int licao_minna_no_nihongo;
                if (values[4].Length > 0)
                {
                    licao_minna_no_nihongo = int.Parse(values[4]);
                }
                else
                {
                    licao_minna_no_nihongo = -1;
                }


                //só quero incluir os partículas do nível especificado
                bool particulaDeveSerIncluidaNoJogo = false;
                for (int m = 0; m < niveis_usar_no_jogo.Length; m++)
                {
                    if (niveis_usar_no_jogo[m] == nivel_no_jogo)
                    {
                        particulaDeveSerIncluidaNoJogo = true;
                        break;
                    }
                }
                if (particulaDeveSerIncluidaNoJogo == true)
                {
                    particulaOuFormaVerbal novoDadoExtraidoDoBanco = new particulaOuFormaVerbal(particula_ou_forma_verbal, estrutura_gramatical, nivel_no_jogo, licao_minna_no_nihongo);
                    if (particula_ou_forma_verbal == "partícula" || particula_ou_forma_verbal == "particula" || particula_ou_forma_verbal == "Partícula")
                    {
                        particulasUsadasNaJogatina.AddLast(novoDadoExtraidoDoBanco);
                    }
                    else
                    {
                        formasVerbaisUsadasNaJogatina.AddLast(novoDadoExtraidoDoBanco);
                    }
                }

            }

           
        }

        Debug.Log("banco_de_dados.cs//particulasUsadasNaJogatina.length=" + particulasUsadasNaJogatina.Count);
        Debug.Log("banco_de_dados.cs//formasVerbaisUsadasNaJogatina.length=" + formasVerbaisUsadasNaJogatina.Count);

    }


    public void parseTextAssetFrasesDoJogo(TextAsset ft)
    {

        string fs = ft.text;
        string[] fLines = Regex.Split(fs, "\n|\r|\r\n");

        for (int i = 0; i < fLines.Length; i++)
        {

            string valueLine = fLines[i];
            if(valueLine.Length > 0)
            {
                string[] values = Regex.Split(valueLine, ";"); // your splitter here
                int idDafrase = int.Parse(values[0]);
                int nivelNoJogo = int.Parse(values[1]);
                string frase = values[2];
                string traducao = values[3];
                string oQueEhFormaVerbal = values[4];
                string resposta = values[5];
                string particulaOuFormaVerbal = values[6];
                int capituloMinnaNoNihongo = int.Parse(values[7]);

                //só quero incluir as frases do nível especificado
                bool fraseDeveSerIncluidaNoJogo = false;
                for (int m = 0; m < niveis_usar_no_jogo.Length; m++)
                {
                    if (niveis_usar_no_jogo[m] == nivelNoJogo)
                    {
                        fraseDeveSerIncluidaNoJogo = true;
                        break;
                    }
                }
                if (fraseDeveSerIncluidaNoJogo == true)
                {
                    fraseDaBarcaca novoDadoExtraidoDoBanco = new fraseDaBarcaca(idDafrase, nivelNoJogo, frase, traducao, oQueEhFormaVerbal, resposta, particulaOuFormaVerbal, capituloMinnaNoNihongo);

                    if (particulaOuFormaVerbal == "partícula" || particulaOuFormaVerbal == "particula" || particulaOuFormaVerbal == "Partícula")
                    {
                        frasesTreinamParticulasUsarNasBarcacas.AddLast(novoDadoExtraidoDoBanco);
                    }
                    else
                    {
                        frasesTreinamFormasVerbaisUsarNasBarcacas.AddLast(novoDadoExtraidoDoBanco);
                    }
                }
            }
            

        }
        Debug.Log("banco_de_dados.cs//frasesTreinamParticulasUsarNasBarcacas.length=" + frasesTreinamParticulasUsarNasBarcacas.Count);
        Debug.Log("banco_de_dados.cs//frasesTreinamFormasVerbaisUsarNasBarcacas.length=" + frasesTreinamFormasVerbaisUsarNasBarcacas.Count);

    }

    public void parseTextAssetVocabularioTreinado(TextAsset ft)
    {

        string fs = ft.text;
        string[] fLines = Regex.Split(fs, "\n|\r|\r\n");

        for (int i = 0; i < fLines.Length; i++)
        {

            string valueLine = fLines[i];
            if (valueLine.Length > 0)
            {
                string[] values = Regex.Split(valueLine, ";"); // your splitter here
                int idDaPalavra = int.Parse(values[0]);
                int idDafrase = int.Parse(values[1]);
                string palavra = values[2];
                string traducao = values[3];
                string pegadinha = values[4];
                int nivelNoJogo = int.Parse(values[5]);
            

                //só quero incluir as frases do nível especificado
                bool palavraDeveSerIncluidaNoJogo = false;
                for (int m = 0; m < niveis_usar_no_jogo.Length; m++)
                {
                    if (niveis_usar_no_jogo[m] == nivelNoJogo)
                    {
                        palavraDeveSerIncluidaNoJogo = true;
                        break;
                    }
                }
                if (palavraDeveSerIncluidaNoJogo == true)
                {
                    palavraVocabulario novoDadoExtraidoDoBanco = new palavraVocabulario(idDaPalavra, idDafrase, palavra, traducao, pegadinha, nivelNoJogo);

                    palavrasTreinadasNoJogo.AddLast(novoDadoExtraidoDoBanco);
                }
            }


        }
        //Debug.Log("banco_de_dados.cs//palavrasTreinadasNoJogo.length=" + palavrasTreinadasNoJogo.Count);
    }

    private particulaOuFormaVerbal particulaOuFormaVerbalEscolhida;
    public void mudarTextoDoSushi(int indiceDoSushi)
    {
        //Debug.Log("banco_de_dados.cs//tentando mudar o texto do sushi=" + indiceDoSushi);
        //escolher uma das particulas/formas verbais aleatoriamente
        LinkedList<particulaOuFormaVerbal> listaUsadaPraMudarOTexto;
        if (script_cronometro.current_timer < 840)//das 8-14h é sushi q é servido(treinando partículas). das 14:01-20h, nigirisushi
        {
            listaUsadaPraMudarOTexto = particulasUsadasNaJogatina;
        }
        else
        {
            listaUsadaPraMudarOTexto = formasVerbaisUsadasNaJogatina;
        }
        int indiceParticulaOuFormaVerbalAleatoria = UnityEngine.Random.Range(0, listaUsadaPraMudarOTexto.Count);
        int percorredorParticulasOuFormasVerbaisAleatorias = 0;
        //Debug.Log("banco_de_dados.cs//indiceParticulaOuFormaVerbalAleatoria=" + indiceParticulaOuFormaVerbalAleatoria);
        //Debug.Log("banco_de_dados.cs//tamanho listaUsadaParaMudarOTExto=" + listaUsadaPraMudarOTexto.Count);
        foreach (var particulaOuFormaVerbalDoJogo in listaUsadaPraMudarOTexto)
        {
            if (percorredorParticulasOuFormasVerbaisAleatorias == indiceParticulaOuFormaVerbalAleatoria)
            {
                particulaOuFormaVerbalEscolhida = particulaOuFormaVerbalDoJogo;
                break;
            }
            else
            {
                percorredorParticulasOuFormasVerbaisAleatorias = percorredorParticulasOuFormasVerbaisAleatorias + 1;
            }
        }

        GameObject gameObjetoTextoSushiMudar = GameObject.Find("texto_sushi_" + indiceDoSushi);
        //Debug.Log("banco_de_dados.cs//achei gameObject do texto da barcaca=" + gameObjetoFraseBarcacaMudar.name);
        texto_sushi_particula objetoTextoBarcaca = gameObjetoTextoSushiMudar.GetComponent<texto_sushi_particula>();
        //Debug.Log("banco_de_dados.cs//frase escolhida=" + fraseBarcacaEscolhida.frase);
        objetoTextoBarcaca.setparticulaOuFormaVerbalAssociado(particulaOuFormaVerbalEscolhida);

    }

    public void mudarTextoDoSushiSemRepetido(int indiceDoSushi, string antigaParticula)
    {
        //Debug.Log("banco_de_dados.cs//tentando mudar o texto do sushi=" + indiceDoSushi);
        //escolher uma das particulas/formas verbais aleatoriamente
        LinkedList<particulaOuFormaVerbal> listaUsadaPraMudarOTexto;
        if (script_cronometro.current_timer < 840)//das 8-14h é sushi q é servido(treinando partículas). das 14:01-20h, nigirisushi
        {
            listaUsadaPraMudarOTexto = particulasUsadasNaJogatina;
        }
        else
        {
            listaUsadaPraMudarOTexto = formasVerbaisUsadasNaJogatina;
        }
        bool acheiParticulaNaoRepetida = false;
        while(acheiParticulaNaoRepetida == false)
        {
            int indiceParticulaOuFormaVerbalAleatoria = UnityEngine.Random.Range(0, listaUsadaPraMudarOTexto.Count);
            int percorredorParticulasOuFormasVerbaisAleatorias = 0;
            //Debug.Log("banco_de_dados.cs//indiceParticulaOuFormaVerbalAleatoria=" + indiceParticulaOuFormaVerbalAleatoria);
            //Debug.Log("banco_de_dados.cs//tamanho listaUsadaParaMudarOTExto=" + listaUsadaPraMudarOTexto.Count);
            foreach (var particulaOuFormaVerbalDoJogo in listaUsadaPraMudarOTexto)
            {
                if (percorredorParticulasOuFormasVerbaisAleatorias == indiceParticulaOuFormaVerbalAleatoria)
                {
                    particulaOuFormaVerbalEscolhida = particulaOuFormaVerbalDoJogo;
                    if(particulaOuFormaVerbalEscolhida.getEstruturaGramatical() != antigaParticula)
                    {
                        acheiParticulaNaoRepetida = true;
                    }
                    break;
                }
                else
                {
                    percorredorParticulasOuFormasVerbaisAleatorias = percorredorParticulasOuFormasVerbaisAleatorias + 1;
                }
            }
        }
       

        GameObject gameObjetoTextoSushiMudar = GameObject.Find("texto_sushi_" + indiceDoSushi);
        //Debug.Log("banco_de_dados.cs//achei gameObject do texto da barcaca=" + gameObjetoFraseBarcacaMudar.name);
        texto_sushi_particula objetoTextoBarcaca = gameObjetoTextoSushiMudar.GetComponent<texto_sushi_particula>();
        //Debug.Log("banco_de_dados.cs//frase escolhida=" + fraseBarcacaEscolhida.frase);
        objetoTextoBarcaca.setparticulaOuFormaVerbalAssociado(particulaOuFormaVerbalEscolhida);

    }

    private fraseDaBarcaca fraseBarcacaEscolhida;
    public void mudarTextoDaBarcaca(int indiceDaBarcaca)
    {
        //Debug.Log("banco_de_dados.cs//tentando mudar o texto da barcaca=" + indiceDaBarcaca);
        //escolher uma das frases aleatoriamente
        LinkedList<fraseDaBarcaca> listaUsadaPraMudarOTexto;
        if (script_cronometro.current_timer < 840)//das 8-14h é sushi q é servido(treinando partículas). das 14:01-20h, nigirisushi
        {
            listaUsadaPraMudarOTexto = frasesTreinamParticulasUsarNasBarcacas;
        }
        else
        {
            listaUsadaPraMudarOTexto = frasesTreinamFormasVerbaisUsarNasBarcacas;
        }

        //temos de escolher uma frase aleatoria do jogo, mas ela não pode ser repetida
        bool escolhidaFraseAleatoriaNaoRepetida = false;
        while(escolhidaFraseAleatoriaNaoRepetida == false)
        {
            int indiceFraseAleatoria = UnityEngine.Random.Range(0, listaUsadaPraMudarOTexto.Count);
            int percorredorFrasesAleatorias = 0;
            //Debug.Log("banco_de_dados.cs//indiceFraseAleatoria=" + indiceFraseAleatoria);
            //Debug.Log("banco_de_dados.cs//tamanho frasesUsarNasBarcacas=" + frasesUsarNasBarcacas.Count);
            foreach (var fraseDaBarcacaNoJogo in listaUsadaPraMudarOTexto)
            {
                if (percorredorFrasesAleatorias == indiceFraseAleatoria)
                {
                    fraseBarcacaEscolhida = fraseDaBarcacaNoJogo;
                    break;
                }
                else
                {
                    percorredorFrasesAleatorias = percorredorFrasesAleatorias + 1;
                }
            }
            //será que a frase escolhida não é repetida?
            int posicaoFraseRepetida = Array.IndexOf(frasesJahNasBarcacas, fraseBarcacaEscolhida.getFrase());
            if(posicaoFraseRepetida <= -1)
            {
                //a frase não é repetida mesmo! podemos sair do loop!
                escolhidaFraseAleatoriaNaoRepetida = true;
                frasesJahNasBarcacas[indiceDaBarcaca - 1] = fraseBarcacaEscolhida.getFrase();
            }
        }


        GameObject gameObjetoDicaBarcacaMudar = GameObject.Find("hint_frase_boat_" + indiceDaBarcaca);
        frase_hint_barco hintBarco  = gameObjetoDicaBarcacaMudar.GetComponent<frase_hint_barco>();
        hintBarco.setFraseAssociada(fraseBarcacaEscolhida);
        GameObject gameObjetoFraseBarcacaMudar = GameObject.Find("frase_sushi_boat_" + indiceDaBarcaca);
        //Debug.Log("banco_de_dados.cs//achei gameObject do texto da barcaca=" + gameObjetoFraseBarcacaMudar.name);
        frase_barco objetoTextoBarcaca = gameObjetoFraseBarcacaMudar.GetComponent<frase_barco>();
        //Debug.Log("banco_de_dados.cs//frase escolhida=" + fraseBarcacaEscolhida.frase);
        objetoTextoBarcaca.setFraseAssociada(fraseBarcacaEscolhida);




    }

    palavraVocabulario palavraEscolhidaTreinar;
    public palavraVocabulario pegarPalavraVocabularioAssociada(int indiceBarcacaInspetorEstah)
    {
        string nomeSushiBoatAssociado = "frase_sushi_boat_" + indiceBarcacaInspetorEstah;
        //Debug.Log("frase_barco.cs///barco_sushi_associado = " + nomeSushiBoatAssociado);
        GameObject objetoComFraseBarco = GameObject.Find(nomeSushiBoatAssociado);
        fraseDaBarcaca fraseDoBarco = objetoComFraseBarco.GetComponent<frase_barco>().getFraseAssociada();
        int idFrase = fraseDoBarco.getId_frase();
        LinkedList<palavraVocabulario> palavrasTreinadasNaFrase = new LinkedList<palavraVocabulario>(); 
        foreach(var umaPalavraTreinadaNoJogo in palavrasTreinadasNoJogo)
        {
            if(idFrase == umaPalavraTreinadaNoJogo.getId_frase())
            {
                //achamos uma palavra do vocabulário associada à frase!
                palavrasTreinadasNaFrase.AddLast(umaPalavraTreinadaNoJogo);
            }
        }

        //agora, escolher uma palavra pra treinar aleatoriamente
        
        int indicePalavraAleatoria = UnityEngine.Random.Range(0, palavrasTreinadasNaFrase.Count);
        int percorredorPalavrasAleatorias = 0;
        foreach (var palavrasDaFrase in palavrasTreinadasNaFrase)
        {
            if (percorredorPalavrasAleatorias == indicePalavraAleatoria)
            {
                palavraEscolhidaTreinar = palavrasDaFrase;
                break;
            }
            else
            {
                percorredorPalavrasAleatorias = percorredorPalavrasAleatorias + 1;
            }
        }

        //faltou só encontrar outras 2 alternativas para treinar junto a essa palavra. elas serão do mesmo nível da palavra escolhida
        int alternativasEncontradas = 0;
        string[] alternativasErradas = new string[2];
        while(alternativasEncontradas < 2)
        {
            int indicePalavraAlternativaErrada = UnityEngine.Random.Range(0, palavrasTreinadasNaFrase.Count);
            int percorredorPalavrasDoJogo = 0;
            if(indicePalavraAlternativaErrada != indicePalavraAleatoria)
            {
                foreach (var palavraDoJogo in palavrasTreinadasNoJogo)
                {
                    if (percorredorPalavrasDoJogo == indicePalavraAlternativaErrada)
                    {
                        if(palavraDoJogo.getNivel_jogo() == palavraEscolhidaTreinar.getNivel_jogo())
                        {
                            //achamos uma palavra aleatoria do jogo compatível! vamos pegar sua traducao e sua pegadinha pra botar como alternativa
                            string traducaoErrada1 = palavraDoJogo.getTraducao();
                            //a alternativa só entra se ela não for a tradução nem a pegadinha da palavra escolhida e nem estah no array
                            if (estahNoArray(traducaoErrada1, alternativasErradas) == false && traducaoErrada1 != palavraEscolhidaTreinar.getTraducao() && traducaoErrada1 != palavraEscolhidaTreinar.getPegadinha())
                            {
                                alternativasErradas[alternativasEncontradas] = traducaoErrada1;
                                alternativasEncontradas = alternativasEncontradas + 1;
                                if(alternativasEncontradas >= 2)
                                {
                                    break;
                                }
                            }
                            string traducaoErrada2 = palavraDoJogo.getPegadinha();
                            if (estahNoArray(traducaoErrada2, alternativasErradas) == false && traducaoErrada2 != palavraEscolhidaTreinar.getTraducao() && traducaoErrada2 != palavraEscolhidaTreinar.getPegadinha())
                            {
                                alternativasErradas[alternativasEncontradas] = traducaoErrada2;
                                alternativasEncontradas = alternativasEncontradas + 1;
                                if (alternativasEncontradas >= 2)
                                {
                                    break;
                                }
                            }
                        }

                    }
                    else
                    {
                        percorredorPalavrasDoJogo = percorredorPalavrasDoJogo + 1;
                    }
                }
            }
           
        }

        palavraEscolhidaTreinar.setTraducoesErradasAlternativasParaJogo(alternativasErradas);

        return palavraEscolhidaTreinar;

    }

    bool estahNoArray(string elemento, string [] array)
    {
        bool estahNoArr = false;
        for(int i = 0; i < array.Length; i++)
        {
            if(elemento == array[i])
            {
                estahNoArr = true;
                break;
            }
        }
        return estahNoArr;
    }
    
	
	// Update is called once per frame
	void Update () {
	
	}

    public class particulaOuFormaVerbal
    {
        
        private string ehParticulaOuFormaVerbal;
        private string estruturaGramatical;
        private int nivelNoJogo;
        private int numeroDaLicaoMinnaNoNihongo;

        public string getEhParticulaOuFormaVerbal()
        {
            return ehParticulaOuFormaVerbal;
        }
        public void setEhParticulaOuFormaVerbal(string novoValor)
        {
            this.ehParticulaOuFormaVerbal = novoValor;
        }
        public string getEstruturaGramatical()
        {
            return estruturaGramatical;
        }
        public void setEstruturaGramatical(string novoValor)
        {
            estruturaGramatical = novoValor;
        }
        public int getNivelNoJogo()
        {
            return nivelNoJogo;
        }
        public void setNivelNoJogo(int novoValor)
        {
            nivelNoJogo = novoValor;
        }
        public int getNumeroDaLicaoMinnaNoNihongo()
        {
            return numeroDaLicaoMinnaNoNihongo;
        }
        public void setNumeroDaLicaoMinnaNoNihongo(int novoValor)
        {
            numeroDaLicaoMinnaNoNihongo = novoValor;
        }


        public particulaOuFormaVerbal(string ehPartOuFormVerbal, string structGramatical, int levelNoJogo, int emQualLicaoMinnaNoNihongo)
        {
            this.ehParticulaOuFormaVerbal = ehPartOuFormVerbal;
            this.estruturaGramatical = structGramatical;
            this.nivelNoJogo = levelNoJogo;
            this.numeroDaLicaoMinnaNoNihongo = emQualLicaoMinnaNoNihongo;
        }

        // Constructor
        public particulaOuFormaVerbal()
        {

        }
    }

    public class fraseDaBarcaca
    {

        private int id_frase; 
        private int nivel_jogo;
        private string frase;
        private string traducao;
        private string dicaFormaVerbal;
        private string resposta;
        private string ehParticulaOuFormaVerbal;
        private int numeroDaLicaoMinnaNoNihongo;

        public int getId_frase()
        {
            return id_frase;
        }

        public int getNivel_jogo()
        {
            return nivel_jogo;
        }
        public void setNivel_jogo(int novoValor)
        {
            nivel_jogo = novoValor;
        }
        public string getFrase()
        {
            return frase;
        }
        public void setfrase(string novoValor)
        {
            frase = novoValor;
        }

        public string getTraducao()
        {
            return traducao;
        }
        public void setTraducao(string novoValor)
        {
            traducao = novoValor;
        }
        public string getDicaFormaVerbal()
        {
            return dicaFormaVerbal;
        }
        public void setDicaFormaVerbal(string novoValor)
        {
            dicaFormaVerbal = novoValor;
        }
        public string getResposta()
        {
            return resposta;
        }
        public void setResposta(string novoValor)
        {
            resposta = novoValor;
        }
        public string getEhParticulaOuFormaVerbal()
        {
            return ehParticulaOuFormaVerbal;
        }
        public void setEhParticulaOuFormaVerbal(string novoValor)
        {
            ehParticulaOuFormaVerbal = novoValor;
        }

        public int getNumeroDaLicaoMinnaNoNihongo()
        {
            return numeroDaLicaoMinnaNoNihongo;
        }
        public void setNumeroDaLicaoMinnaNoNihongo(int novoValor)
        {
            numeroDaLicaoMinnaNoNihongo = novoValor;
        }


        public fraseDaBarcaca(int idDafrase, int nivelNoJogo, string textoFrase, string textoTRaducao, string dicaDaFormVerbal, string textoResposta, string particulaOuFormaVerbal, int capituloDoMinnaNoNihongo)
        {
            this.id_frase = idDafrase;
            this.nivel_jogo = nivelNoJogo;
            this.frase = textoFrase;
            this.traducao = textoTRaducao;
            this.dicaFormaVerbal = dicaDaFormVerbal;
            this.resposta = textoResposta;
            this.ehParticulaOuFormaVerbal = particulaOuFormaVerbal;
            this.numeroDaLicaoMinnaNoNihongo = capituloDoMinnaNoNihongo;
        }

        // Constructor
        public fraseDaBarcaca()
        {

        }
    }

    public class palavraVocabulario
    {

        private int id_frase;
        private int id_palavra;
        private string palavra;
        private string traducao;
        private string pegadinha;
        private int nivel_jogo;
        private string [] traducoesErradasAlternativasParaJogo;//fica com as outras 2 alternativas usadas no minijogo

        public int getId_frase()
        {
            return id_frase;
        }

        public int getId_palavra()
        {
            return id_palavra;
        }
        public void setId_palavra(int novoValor)
        {
            id_palavra = novoValor;
        }
        public string getPalavra()
        {
            return palavra;
        }
        public void setPalavra(string novoValor)
        {
            palavra = novoValor;
        }

        public string getTraducao()
        {
            return traducao;
        }
        public void setTraducao(string novoValor)
        {
            traducao = novoValor;
        }
        public string getPegadinha()
        {
            return pegadinha;
        }
        public void setPegadinha(string novoValor)
        {
            pegadinha = novoValor;
        }

        public int getNivel_jogo()
        {
            return nivel_jogo;
        }
        public void setNivel_jogo(int novoValor)
        {
            nivel_jogo = novoValor;
        }

        public string [] getTraducoesErradasAlternativasParaJogo()
        {
            return traducoesErradasAlternativasParaJogo;
        }
        public void setTraducoesErradasAlternativasParaJogo( string [] novoValor)
        {
            traducoesErradasAlternativasParaJogo = novoValor;
        }




        public palavraVocabulario(int idDaPalavra, int idDafrase, string palavraTreinada, string traducaoTreinada, string pegadinhaAssociada, int nivelNoJogo)
        {
            this.id_frase = idDafrase;
            this.id_palavra = idDaPalavra;
            this.palavra = palavraTreinada;
            this.traducao = traducaoTreinada;
            this.pegadinha = pegadinhaAssociada;
            this.nivel_jogo = nivelNoJogo;
        }

        // Constructor
        public palavraVocabulario()
        {

        }
    }
}
