﻿using UnityEngine;
using System.Collections;

public class anzol_script : MonoBehaviour
{
    GameObject objetoLinhaDoAnzol;

    // Use this for initialization
    void Start()
    {
        objetoLinhaDoAnzol = GameObject.Find("linha_hook");
        //objetoLinhaDoAnzol.transform.position = new Vector3(1.11f, 4.39f, 0f);
        //gameObject.transform.position = new Vector3(0.92f, -0.06f, 0f);


    }



    void OnCollisionStay2D(Collision2D coll)
    {
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name.Contains("peixe") == true)
        {
            Debug.Log("anzol_script.cs//aparece, hint bubble!!!!");
            GameObject alertaDePeixe = GameObject.Find("speech_bubble_alerta_peixe");
            alertaDePeixe.GetComponent<Renderer>().enabled = true;
        }
        else if (col.gameObject.name.Contains("tubarao") == true)
        {
            GameObject.Find("background_minijogo_pegar_sushi").GetComponent<minijogo_pegar_peixe>().acabarMinijogoPorAtaqueDeTubarao();
        }

    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.name.Contains("peixe") == true && Input.GetMouseButtonDown(0) == true)
        {
            Debug.Log("anzol_script.cs//usuário pegou um peixe!");
            peixeOuTubaraoScript peixe = col.gameObject.GetComponent<peixeOuTubaraoScript>();
            bool peixeFoiPego = peixe.getPeixeFoiPego();
            if (peixeFoiPego == false)
            {
                peixe.setPeixeFoiPego(true);
                col.transform.parent = gameObject.transform;
            }
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.name.Contains("peixe") == true)
        {
            GameObject alertaDePeixe = GameObject.Find("speech_bubble_alerta_peixe");
            alertaDePeixe.GetComponent<Renderer>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(gameObject.transform.position.x != 0.92f)
        {

        }


    }

    IEnumerator fazerOBarcoDesaparecer()
    {
        yield return new WaitForSeconds(1);

    }



}
