﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class minijogo_pegar_peixe : MonoBehaviour {

    public int tempoMinijogo;
    private int segundosSemPeixes;
    GameObject objetoAnzol;
    GameObject objetoTextoTempoRestanteMinijogo;
    volatile bool tempoMinijogoAcabou;
    volatile int quantosPeixesPegou;
    public AudioClip audioAtaqueDeTubarao;
    private bool anzolArrebentou;

    // Use this for initialization
    void Start () {
        objetoAnzol = GameObject.Find("fish_hook");
        objetoTextoTempoRestanteMinijogo = GameObject.Find("tempo_restante_minijogo");
	
	}

    public void comecarPartida()
    {
        tempoMinijogo = 60;
        segundosSemPeixes = 0;
        quantosPeixesPegou = 0;
        tempoMinijogoAcabou = false;
        anzolArrebentou = false;
        GameObject.Find("tempo_restante_minijogo").GetComponent<Text>().text = "" + tempoMinijogo;
       
        
    }

    public void iniciarCronometroDoJogo()
    {
        StartCoroutine(passarTempoMinijogo());
    }

    public void aumentarQuantosPeixesPegou()
    {
        quantosPeixesPegou = quantosPeixesPegou + 1;
        GameObject.Find("texto_quantos_peixes_pegou").GetComponent<Text>().text = "X " + quantosPeixesPegou;
        Debug.Log("minijogo_pegar_peixe.cs//usuário pegou mais um peixe!qnt=" + quantosPeixesPegou);
    }

    IEnumerator passarTempoMinijogo()
    {
        while(tempoMinijogoAcabou == false)
        {
            int numeroAleatorio = Random.Range(0, 101);
            if(numeroAleatorio % 2 == 0 && segundosSemPeixes >= 1)
            {
                segundosSemPeixes = 0;
                //gerar um objeto peixe ou tubarão
                int numeroAleatorioPeixeOuTubarao  = Random.Range(0, 101);
                GameObject peixeOuTubaraoClonado;
                if(numeroAleatorioPeixeOuTubarao % 2 == 0)
                {
                    peixeOuTubaraoClonado = GameObject.Find("peixe");
                }
                else
                {
                    peixeOuTubaraoClonado = GameObject.Find("tubarao");
                }
                Vector3 posicaoInicialPeixe = peixeOuTubaraoClonado.transform.position;
                GameObject novoPeixeOuTubarao = (GameObject)Instantiate(peixeOuTubaraoClonado, posicaoInicialPeixe, peixeOuTubaraoClonado.transform.rotation);
                novoPeixeOuTubarao.transform.SetParent(gameObject.transform);
                novoPeixeOuTubarao.GetComponent<peixeOuTubaraoScript>().enabled = true;
                
            }
            yield return new WaitForSeconds(1);
            //Debug.Log("minijogo_pegar_peixe.cs// novos segundos =" + tempoMinijogo);
            tempoMinijogo = tempoMinijogo - 1;
            segundosSemPeixes = segundosSemPeixes + 1;
            objetoTextoTempoRestanteMinijogo.GetComponent<Text>().text = "" + tempoMinijogo;
            if (tempoMinijogo == 0)
            {
                tempoMinijogoAcabou = true;
            }
        }
        

    }

    public void acabarMinijogoPorAtaqueDeTubarao()
    {
        AudioSource.PlayClipAtPoint(audioAtaqueDeTubarao, gameObject.transform.position);
        this.tempoMinijogoAcabou = true;
    }

    public void acabarMinijogoPorFishhookArrebentado()
    {
        this.anzolArrebentou = true;
        this.tempoMinijogoAcabou = true;
    }


    // Update is called once per frame
    void Update()
    {
        bool jogoComecou = GameObject.Find("contador_sushis").GetComponent<contador_sushis_script>().getEhHoraDoMinijogoPegarPeixe();
        //Debug.Log("minijogo_pegar_peixe.cs//jogo comecou=" + jogoComecou);
        if (jogoComecou == true && tempoMinijogoAcabou == true)
        {
            StopCoroutine("passarTempoMinijogo");
            GameObject.Find("contador_sushis").GetComponent<contador_sushis_script>().terminarMinijogo(quantosPeixesPegou, anzolArrebentou);
        }
        else 
        {
            if(jogoComecou == true && Input.GetMouseButtonDown(0) == true && objetoAnzol.GetComponent<Renderer>().isVisible)
            {
                Debug.Log("minijogo_pegar_peixe.cs//anzol pra cima!");
                GameObject objetoLinhaAnzol = GameObject.Find("linha_hook");
                objetoLinhaAnzol.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                objetoLinhaAnzol.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, 10, 0), ForceMode2D.Impulse);
                objetoAnzol.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                objetoAnzol.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, 10, 0), ForceMode2D.Impulse);
            }
            
        }
        
        

    }
}
