﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class chao_script : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.name == "fish_hook" || col.gameObject.name == "linha_hook")
        {
            //Debug.Log("chao_script.cs//objeto que colidiu com chao=" + col.gameObject.name);
            col.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
            GameObject objetoLinhaAnzol = GameObject.Find("linha_hook");
            objetoLinhaAnzol.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        }
        
    }
}
