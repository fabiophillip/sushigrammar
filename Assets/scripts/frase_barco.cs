﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class frase_barco : MonoBehaviour {

    private banco_de_dados.fraseDaBarcaca fraseAssociada;
    GameObject barcoSeguir;

    // Use this for initialization
    void Start () {
        string nomeObjeto = gameObject.transform.name;
        string[] nome_splitado = nomeObjeto.Split('_');
        string numeroDaFraseBarco = nome_splitado[nome_splitado.Length - 1];
        string nomeSushiBoatAssociado = "sushi_boat_" + numeroDaFraseBarco;
        //Debug.Log("frase_barco.cs///barco_sushi_associado = " + nomeSushiBoatAssociado);
        barcoSeguir = GameObject.Find(nomeSushiBoatAssociado);
        Vector3 posicaoBackground = barcoSeguir.transform.position;
        posicaoBackground.x = posicaoBackground.x + 0.4f;
        this.gameObject.transform.position = posicaoBackground;

    }

    public banco_de_dados.fraseDaBarcaca getFraseAssociada()
    {
        return this.fraseAssociada;
    }

    public void setFraseAssociada(banco_de_dados.fraseDaBarcaca novoValor)
    {
        fraseAssociada = novoValor;
        //falta mudar o próprio texto da frase do barco
        string textoDoBarco = fraseAssociada.getFrase();
        //Debug.Log("frase_barco.cs//texto do barco " + textoDoBarco);
        string textoDoBarcoComKanjisNumeroParticula = "";
        int numeroParticulaNoTextoSushi = 1;
        for (int k = 0; k < textoDoBarco.Length; k++)
        {
            if (textoDoBarco[k] != '＿')
            {
                textoDoBarcoComKanjisNumeroParticula = textoDoBarcoComKanjisNumeroParticula + textoDoBarco[k];
            }
            else
            {
                if (numeroParticulaNoTextoSushi == 1)
                {
                    textoDoBarcoComKanjisNumeroParticula = textoDoBarcoComKanjisNumeroParticula + " <color=##ff0000ff>一</color> ";
                }
                else if (numeroParticulaNoTextoSushi == 2)
                {
                    textoDoBarcoComKanjisNumeroParticula = textoDoBarcoComKanjisNumeroParticula + " <color=##ff0000ff>二</color> ";
                }
                else if (numeroParticulaNoTextoSushi == 3)
                {
                    textoDoBarcoComKanjisNumeroParticula = textoDoBarcoComKanjisNumeroParticula + " <color=##ff0000ff>三</color> ";
                }
                else
                {
                    textoDoBarcoComKanjisNumeroParticula = textoDoBarcoComKanjisNumeroParticula + " <color=##ff0000ff>四</color> ";
                }
                numeroParticulaNoTextoSushi = numeroParticulaNoTextoSushi + 1;
            }
        }
        
        GetComponent<Text>().text = textoDoBarcoComKanjisNumeroParticula;

        //falta só mudar quantos sushis são necessários para a frase no barco_sushi associado

        //Debug.Log("barco_sushi// start");
        string nomeObjeto = gameObject.transform.name;
        string[] nome_splitado = nomeObjeto.Split('_');
        string numeroDoTextoBarco = nome_splitado[nome_splitado.Length - 1];
        string nomeObjetoBarco = "sushi_boat_" + numeroDoTextoBarco;
        GameObject objetoBarco = GameObject.Find(nomeObjetoBarco);
        barco_sushi barcoDeSushi = objetoBarco.GetComponent<barco_sushi>();
        int quantosSushisNecessariosFrase = numeroParticulaNoTextoSushi - 1;
        barcoDeSushi.setQuantosSushisNecessariosPraFrase(quantosSushisNecessariosFrase); 
        //Debug.Log("frase_barco.cs// quantos sushis para a frase " + textoDoBarcoComKanjisNumeroParticula + ";= " + quantosSushisNecessariosFrase);


    }

    // Update is called once per frame
    void Update () {
        if (barcoSeguir != null)
        {
            Vector3 posicaoBarco = barcoSeguir.transform.position;
            this.gameObject.transform.position = posicaoBarco;
        }


    }
}
