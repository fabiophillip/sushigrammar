﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class money_sushi : MonoBehaviour {

    private Text textoMoneySushi;
    private double moneyAtual;
    private bool mudarMoney;
    private double moneyAtualizado;


	// Use this for initialization
	void Start () {
        GameObject objetoTextoMoneySushi = GameObject.Find("texto_money_sushi");
        textoMoneySushi = objetoTextoMoneySushi.GetComponent<Text>();
        textoMoneySushi.text = "R$ 0.00";
        moneyAtual = 0.0d;
        mudarMoney = false;

	
	}

    public double getMoneyAtual()
    {
        return moneyAtual;
    }

    public void aumentarDinheiro(double aumentarEmQuanto)
    {
        moneyAtualizado = moneyAtual + aumentarEmQuanto;
        mudarMoney = true;

    }
	
	// Update is called once per frame
	void Update () {
        if(mudarMoney == true)
        {

            moneyAtual = moneyAtual + 0.10d;
            double moneyArrendondado = Math.Round(moneyAtual, 2);
            textoMoneySushi.text = "R$ " + moneyArrendondado.ToString("F2");
            if(moneyAtual >= moneyAtualizado)
            {
                mudarMoney = false;
                moneyAtual = moneyAtualizado;
                textoMoneySushi.text = "R$ " + moneyAtual.ToString("F2");
            }
        }
	
	}
}
