﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Data;
using System;

public class texto_sushi_particula : MonoBehaviour {

    GameObject sushiSeguir;
    private banco_de_dados.particulaOuFormaVerbal particulaOuFormaVerbalAssociado;

    public banco_de_dados.particulaOuFormaVerbal getParticulaOuFormaVerbalAssociado()
    {
        return particulaOuFormaVerbalAssociado;
    }
    public void setparticulaOuFormaVerbalAssociado(banco_de_dados.particulaOuFormaVerbal novoValor)
    {
        particulaOuFormaVerbalAssociado = novoValor;
        //falta mudar o próprio texto do sushi

        string textoDoSushi = particulaOuFormaVerbalAssociado.getEstruturaGramatical();
       
        GetComponent<Text>().text = textoDoSushi;
        reduzirOuAumentarFonteDeAcordoComTexto();
    }

    // Use this for initialization
    void Start()
    {
        string nomeObjeto = gameObject.transform.name;
        string[] nome_splitado = nomeObjeto.Split('_');
        string numeroDoTextoSushi = nome_splitado[nome_splitado.Length - 1];
        string nomeSushiAssociado = "sushi_" + numeroDoTextoSushi;
        //Debug.Log("particula_sushi.cs///sushi_associado = " + nomeSushiAssociado);
        sushiSeguir = GameObject.Find(nomeSushiAssociado);
        //pegar o texto da partícula pra ver quão grande é a partícula. dependendo do tamanho, pode ser necessário reduzir/aumentar fonte
        reduzirOuAumentarFonteDeAcordoComTexto();
    }
    private void reduzirOuAumentarFonteDeAcordoComTexto()
    {
        Text objetoTextoParticula = this.gameObject.GetComponent<Text>();

        string textoParticula = objetoTextoParticula.text;
        //Debug.Log("texto_sushi_particula//partícula=" + textoParticula);
        Font fontAntiga = objetoTextoParticula.font;
        if (textoParticula.Length == 1)
        {
            //Debug.Log("texto_sushi_particula//length=1");
            objetoTextoParticula.fontSize = 21;

        }
        else if (textoParticula.Length == 2)
        {
            //Debug.Log("texto_sushi_particula//length=2");
            objetoTextoParticula.fontSize = 19;
        }
        else if (textoParticula.Length == 3)
        {
            //Debug.Log("texto_sushi_particula//length=3");
            objetoTextoParticula.fontSize = 17;
        }
        else if (textoParticula.Length == 4)
        {
            //Debug.Log("texto_sushi_particula//length=4");
            objetoTextoParticula.fontSize = 14;
        }
        else
        {
            //Debug.Log("texto_sushi_particula//length=5");
            objetoTextoParticula.fontSize = 11;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }
}
