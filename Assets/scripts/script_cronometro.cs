﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class script_cronometro : MonoBehaviour
{
    Text texto;
    public static volatile bool tempo_ativo;
    public static float current_timer;
    bool iniciou_hora_do_nigirizushi;
    contador_sushis_script sabeHoraDoMinijogo;
    GameObject backgroundResultsMinijogo;

    // Use this for initialization
    void Start()
    {
        texto = GetComponent<Text>();
        tempo_ativo = true;
        iniciou_hora_do_nigirizushi = false;
        sabeHoraDoMinijogo = GameObject.Find("contador_sushis").GetComponent<contador_sushis_script>();
        backgroundResultsMinijogo = GameObject.Find("background_results_minigame");
        current_timer = 580;//jogo começa em 08:00 que é a hora de trabalho do jurandir 480 hora boa pra testar hora do nigirizushi = 800
        float minutes = Mathf.Floor(current_timer / 60);
        float seconds = current_timer % 60;
        texto.text = minutes + ":" + Mathf.RoundToInt(seconds);
        GameObject backgroundPegarPosicaoRelativa = GameObject.Find("background_relogio");
        Vector3 posicaoBackground = backgroundPegarPosicaoRelativa.transform.position;
        this.gameObject.transform.position = posicaoBackground;
        InvokeRepeating("passarUmSegundo", 0, 0.5f);
        print(posicaoBackground);


    }
    void passarUmSegundo()
    {
        if (tempo_ativo == true)
        {
            current_timer = current_timer + 1;
            string minutes = Mathf.Floor(current_timer / 60).ToString("00");
            float seconds = current_timer % 60;

            texto.text = minutes + ":" + Mathf.RoundToInt(seconds).ToString("00");
            //antes o inspetor chegava no horario 240: 12:00 e 16:00
            if(current_timer > 480 && current_timer % 120 == 0 && inspetor_script.inspetorVisivel == false)
            {
                inspetor_script inspetor = GameObject.Find("inspetor_comecar_minijogo").GetComponent<inspetor_script>();
                inspetor.ativarInspetor();
            }

            if (current_timer >= 840 && iniciou_hora_do_nigirizushi == false && sabeHoraDoMinijogo.getEhHoraDoMinijogoPegarPeixe() == false && backgroundResultsMinijogo.GetComponent<Renderer>().isVisible == false)
            {
                iniciou_hora_do_nigirizushi = true;

                //fazer aparecer a tela de trocando comida
                GameObject backgroundHoraTrocarComida = GameObject.Find("background_hora_trocar_comida");
                backgroundHoraTrocarComida.GetComponent<Renderer>().enabled = true;
                Renderer[] renderersSpritesTelaTrocandoComida = backgroundHoraTrocarComida.transform.GetComponentsInChildren<Renderer>();
                for (int k = 0; k < renderersSpritesTelaTrocandoComida.Length; k++)
                {
                    renderersSpritesTelaTrocandoComida[k].enabled = true;
                }
                GameObject textoTituloHoraTrocarComida = GameObject.Find("cabecalho_hora_trocar_comida");
                textoTituloHoraTrocarComida.GetComponent<Text>().enabled = true;
                Text[] textosTelaTrocarComida = textoTituloHoraTrocarComida.transform.GetComponentsInChildren<Text>();
                for (int k = 0; k < textosTelaTrocarComida.Length; k++)
                {
                    textosTelaTrocarComida[k].enabled = true;
                }

                //trocar frases de barcos
                banco_de_dados bancoDeDados = GameObject.Find("banco_de_dados").GetComponent<banco_de_dados>();
                barco_sushi barco_sushi1 = GameObject.Find("sushi_boat_1").GetComponent<barco_sushi>();
                if(barco_sushi1.getBarcoSumindo() == false)
                {
                    bancoDeDados.mudarTextoDaBarcaca(1);
                }
                barco_sushi barco_sushi2 = GameObject.Find("sushi_boat_2").GetComponent<barco_sushi>();
                if (barco_sushi2.getBarcoSumindo() == false)
                {
                    bancoDeDados.mudarTextoDaBarcaca(2);
                }
                barco_sushi barco_sushi3 = GameObject.Find("sushi_boat_3").GetComponent<barco_sushi>();
                if (barco_sushi3.getBarcoSumindo() == false)
                {
                    bancoDeDados.mudarTextoDaBarcaca(3);
                }

                //matar sushis
                GameObject[] objetosSushi = GameObject.FindGameObjectsWithTag("sushi_object");
                for(int i = 0; i < objetosSushi.Length; i++)
                {
                    GameObject umObjetoSushi = objetosSushi[i];
                    if(umObjetoSushi != null)
                    {
                        sushi_script umSushi = umObjetoSushi.GetComponent<sushi_script>();
                        if(umSushi.getSushiEmCimaDeBarco() == true)
                        {
                            //sushi em cima de um barco
                            if(umSushi.getBarcoQueSushiFicaEmCima().getBarcoSumindo() == false)
                            {
                                Destroy(umObjetoSushi);
                            }
                            
                        }
                        else
                        {
                            Destroy(umObjetoSushi);
                        }
                    }
                    
                }
                //gerar novos sushis em cima de chapa
                for(int y = 1; y <= 3; y++)
                {
                    string nomeObjetoNaPosicaoSushi = "dummy_posicao_nigiri_" + y;
                    GameObject objetoPosicaoSemSushi = GameObject.Find(nomeObjetoNaPosicaoSushi);
                    Vector3 posicaoSemSushi = objetoPosicaoSemSushi.transform.position;
                    //criar um novo sushi
                    GameObject objetoSushiPraClonar = GameObject.Find("sushi_8");
                    GameObject novoSushi = (GameObject)Instantiate(objetoSushiPraClonar, posicaoSemSushi, objetoSushiPraClonar.transform.rotation);
                    //novo sushi tem de estar visível
                    novoSushi.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1.0f);
                    novoSushi.transform.parent = objetoSushiPraClonar.transform.parent;
                    novoSushi.name = "sushi_" + contador_sushis_script.idNovosSushisDoJogo;
                    novoSushi.tag = "sushi_object";
                    novoSushi.transform.localScale = new Vector3(73.97274f, 73.97274f);
                    foreach (Transform child in novoSushi.transform)
                    {
                        string antigoNomeFilhoHierarquico = child.name;
                        string[] splitNomeFilhoHierarquico = antigoNomeFilhoHierarquico.Split('_');
                        string novoNomeFilhoHierarquico = "";
                        for (int i = 0; i < splitNomeFilhoHierarquico.Length - 1; i++)
                        {
                            novoNomeFilhoHierarquico = novoNomeFilhoHierarquico + splitNomeFilhoHierarquico[i] + "_";
                        }
                        novoNomeFilhoHierarquico = novoNomeFilhoHierarquico + contador_sushis_script.idNovosSushisDoJogo;
                        child.name = novoNomeFilhoHierarquico;
                        if (novoNomeFilhoHierarquico.Contains("ordem_"))
                        {
                            child.GetComponent<Text>().enabled = false;
                        }

                    }
                    //e falta setar um novo texto para esse novo sushi
                    bancoDeDados.mudarTextoDoSushi(contador_sushis_script.idNovosSushisDoJogo);

                    contador_sushis_script.idNovosSushisDoJogo = contador_sushis_script.idNovosSushisDoJogo + 1;
                }

                




                StartCoroutine(passarTempoPleaseWaitTrocandoSushis());

            }
        }
        if (current_timer > 1199)
        {
            //dia de jurandyr acabou
            tempo_ativo = false;
            money_sushi moneyGanhoUsuario = GameObject.Find("texto_money_sushi").GetComponent<money_sushi>();
            double quantoMoneyGanhou = moneyGanhoUsuario.getMoneyAtual();
            objetivo_do_dia objetivoDoDia = GameObject.Find("texto_objetivo_do_dia").GetComponent<objetivo_do_dia>();
            string metaDoDia = objetivoDoDia.getMetadoDia();
            int valorMeta = objetivoDoDia.getValorMetaDoDia();
            PlayerPrefs.SetString("dinheiro_ganho_na_partida", quantoMoneyGanhou.ToString("F2"));
            PlayerPrefs.SetInt("valor_meta_do_dia", valorMeta);
            PlayerPrefs.SetString("meta_do_dia", metaDoDia);
            SceneManager.LoadScene("fim_de_jogo");
        }
    }


    // Update is called once per frame
    void Update()
    {


    }

    IEnumerator passarTempoPleaseWaitTrocandoSushis()
    {
        int quantosSegundosRestam = 4;
        while (quantosSegundosRestam > 0)
        {
            yield return new WaitForSeconds(1);
            quantosSegundosRestam = quantosSegundosRestam - 1;
            
        }

        //fazer desaparecer a tela de trocando comida
        GameObject backgroundHoraTrocarComida = GameObject.Find("background_hora_trocar_comida");
        backgroundHoraTrocarComida.GetComponent<Renderer>().enabled = false;
        Renderer[] renderersSpritesTelaTrocandoComida = backgroundHoraTrocarComida.transform.GetComponentsInChildren<Renderer>();
        for (int k = 0; k < renderersSpritesTelaTrocandoComida.Length; k++)
        {
            renderersSpritesTelaTrocandoComida[k].enabled = false;
        }
        GameObject textoTituloHoraTrocarComida = GameObject.Find("cabecalho_hora_trocar_comida");
        textoTituloHoraTrocarComida.GetComponent<Text>().enabled = false;
        Text[] textosTelaTrocarComida = textoTituloHoraTrocarComida.transform.GetComponentsInChildren<Text>();
        for (int k = 0; k < textosTelaTrocarComida.Length; k++)
        {
            textosTelaTrocarComida[k].enabled = false;
        }
    }
}
