﻿using UnityEngine;
using System.Collections;

public class particula_script : MonoBehaviour {

    public class particulaOuScript
    {
        public int bullets;
        public int grenades;
        public int rockets;
        public float fuel;

        public string ehParticulaOuFormaVerbal { get; set; }
        public string estruturaGramatical { get; set; }
        public int nivelNoJogo { get; set; }
        public int numeroDaLicaoMinnaNoNihongo { get; set; }
        
        public particulaOuScript(string ehPartOuFormVerbal, string structGramatical, int levelNoJogo, int emQualLicaoMinnaNoNihongo)
        {
            this.ehParticulaOuFormaVerbal = ehPartOuFormVerbal;
            this.estruturaGramatical = structGramatical;
            this.nivelNoJogo = levelNoJogo;
            this.numeroDaLicaoMinnaNoNihongo = emQualLicaoMinnaNoNihongo;
        }

        // Constructor
        public particulaOuScript()
        {
            
        }
    }
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
