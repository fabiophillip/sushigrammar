﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class fim_de_jogo : MonoBehaviour {

	// Use this for initialization
	void Start () {

        string quantoMoneyGanhou = PlayerPrefs.GetString("dinheiro_ganho_na_partida");
        int valorMeta = PlayerPrefs.GetInt("valor_meta_do_dia");
        string metaDoDia = PlayerPrefs.GetString("meta_do_dia");
        GameObject.Find("label_dinheiro_ganho").GetComponent<Text>().text = "Dinheiro ganho\n R$" + quantoMoneyGanhou;
        if (metaDoDia.Contains("dinheiro") == true)
        {
            double dinheiroConvertido = double.Parse(quantoMoneyGanhou);
            int dinheiroConvertidoEmInt = (int)dinheiroConvertido;
            GameObject.Find("label_objetivo_do_dia").GetComponent<Text>().text = "Objetivo do dia\n" + dinheiroConvertidoEmInt + " / " + valorMeta;
            Text textoResultadoDoDia = GameObject.Find("label_resultado_do_dia").GetComponent<Text>();
            if (dinheiroConvertidoEmInt >= valorMeta)
            {
                textoResultadoDoDia.text = "Objetivo cumprido! Parabéns! Ganhou uma página do diário de Jurandir!";
            }
            else
            {
                textoResultadoDoDia.text = "Não foi dessa vez.\nMas amanhã é outro dia!";
            }
        }

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void jogarNovamente()
    {
        Debug.Log("fim_de_jogo.cs//jogar novamente foi acionado");
        SceneManager.LoadScene("main_game");
    }

    public void retry()
    {
        Debug.Log("fim_de_jogo.cs//jogar novamente foi acionado");
        SceneManager.LoadScene(0);
    }
}
