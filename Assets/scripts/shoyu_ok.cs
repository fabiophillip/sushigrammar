﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class shoyu_ok : MonoBehaviour {
    bool botaoAtivo;
    GameObject objetoTextoBotaoOk;//o texto que fica por cima do botão
    barco_sushi barcoAssociadoAoBotaoOk;//barco associado ao botao

    // Use this for initialization
    void Start()
    {
        //botao começa invisivel
        this.GetComponent<Image>().enabled = false;
        botaoAtivo = false;
        string nomeObjeto = gameObject.transform.name;
        string[] nome_splitado = nomeObjeto.Split('_');
        string numeroDoBotaoOk = nome_splitado[nome_splitado.Length - 1];
        string nomeTextoBotaoOk = "ok_button_boat_" + numeroDoBotaoOk;
        objetoTextoBotaoOk = GameObject.Find(nomeTextoBotaoOk);
        //inicialmente, o texto OK fica invisível
        objetoTextoBotaoOk.GetComponent<Text>().enabled = false;


        //tem que achar a barcaca associada a esse botao
        string nomeSushiBoatAssociado = "sushi_boat_" + numeroDoBotaoOk;
        GameObject objetoBarcacaAssociada = GameObject.Find(nomeSushiBoatAssociado);
        barcoAssociadoAoBotaoOk = objetoBarcacaAssociada.GetComponent<barco_sushi>();

        


    }

    // Update is called once per frame
    void Update()
    {
        if (botaoAtivo == true)
        {
            Vector2 p = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D[] hits = Physics2D.OverlapPointAll(p);
            bool mouseEmCimaBotaoOk = false;
            for (int k = 0; k < hits.Length; k++)
            {
                if (hits[k].gameObject == this.gameObject)
                {
                    //o mouse está em cima do inspetor
                    mouseEmCimaBotaoOk = true;
                    Debug.Log("botao_ok_barcaca.cs//mouse em cima de ok");
                    objetoTextoBotaoOk.GetComponent<Text>().fontSize = 22;

                }

            }
            if (mouseEmCimaBotaoOk == false)
            {
                objetoTextoBotaoOk.GetComponent<Text>().fontSize = 15;
            }
        }
    }



    

    public void apertarBotao()
    {
        if (botaoAtivo == true)
        {
            this.barcoAssociadoAoBotaoOk.esvaziarBarcacaESomarPontos();
        }
    }

    //chamado pelo barco_sushi associado a esse botao
    public void ativarBotao()
    {
        this.GetComponent<Image>().enabled = true;
        objetoTextoBotaoOk.GetComponent<Text>().enabled = true;
        botaoAtivo = true;

    }

    public void desativarBotao()
    {
        this.GetComponent<Image>().enabled = false;
        objetoTextoBotaoOk.GetComponent<Text>().enabled = false;
        botaoAtivo = false;
    }

}
