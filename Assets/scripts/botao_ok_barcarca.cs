﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class botao_ok_barcarca : MonoBehaviour {

    bool botaoAtivo;
    GameObject objetoTextoBotaoOk;//o texto que fica por cima do botão
    barco_sushi barcoAssociadoAoBotaoOk;//barco associado ao botao
    private Sprite shoyuNormal;
    private Sprite shoyuBrilhando;

	// Use this for initialization
	void Start () {
        //botao começa invisivel
        this.GetComponent<Renderer>().enabled = false;
        botaoAtivo = false;
        string nomeObjeto = gameObject.transform.name;
        string[] nome_splitado = nomeObjeto.Split('_');
        string numeroDoBotaoOk = nome_splitado[nome_splitado.Length - 1];
        string nomeTextoBotaoOk = "ok_button_boat_" + numeroDoBotaoOk;
        objetoTextoBotaoOk = GameObject.Find(nomeTextoBotaoOk);
        //inicialmente, o texto OK fica invisível
        objetoTextoBotaoOk.GetComponent<Text>().enabled = false;


        //tem que achar a barcaca associada a esse botao
        string nomeSushiBoatAssociado = "sushi_boat_" + numeroDoBotaoOk;
        GameObject objetoBarcacaAssociada = GameObject.Find(nomeSushiBoatAssociado);
        barcoAssociadoAoBotaoOk = objetoBarcacaAssociada.GetComponent<barco_sushi>();

        //e carregar os sprites de shoyu aceso e shoyu normal
        Texture2D texture = Resources.Load("shoyubrilhando") as Texture2D;
        shoyuBrilhando = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));

        Texture2D texture2 = Resources.Load("shoyu") as Texture2D;
        shoyuNormal = Sprite.Create(texture2, new Rect(0.0f, 0.0f, texture2.width, texture2.height), new Vector2(0.5f, 0.5f));




    }

    // Update is called once per frame
    void Update()
    {
        if (botaoAtivo == true)
        {
            Vector2 p = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D[] hits = Physics2D.OverlapPointAll(p);
            bool mouseEmCimaBotaoOk = false;
            for (int k = 0; k < hits.Length; k++)
            {
                if (hits[k].gameObject == this.gameObject)
                {
                    //o mouse está em cima do inspetor
                    mouseEmCimaBotaoOk = true;
                    Debug.Log("botao_ok_barcaca.cs//mouse em cima de ok");
                    objetoTextoBotaoOk.GetComponent<Text>().fontSize = 22;
                    GetComponent<SpriteRenderer>().sprite = shoyuBrilhando;

                }

            }
            if(mouseEmCimaBotaoOk == false)
            {
                objetoTextoBotaoOk.GetComponent<Text>().fontSize = 15;
                GetComponent<SpriteRenderer>().sprite = shoyuNormal;
            }
        }
    }

    

    void OnMouseDown()
    {
        if (botaoAtivo == true)
        {
            this.barcoAssociadoAoBotaoOk.esvaziarBarcacaESomarPontos();
        }
    }

    public void apertarBotao()
    {
        if (botaoAtivo == true)
        {
            this.barcoAssociadoAoBotaoOk.esvaziarBarcacaESomarPontos();
        }
    }

    //chamado pelo barco_sushi associado a esse botao
    public void ativarBotao()
    {
        this.GetComponent<Renderer>().enabled = true;
        objetoTextoBotaoOk.GetComponent<Text>().enabled = true;
        botaoAtivo = true;

    }

    public void desativarBotao()
    {
        this.GetComponent<Renderer>().enabled = false;
        objetoTextoBotaoOk.GetComponent<Text>().enabled = false;
        botaoAtivo = false;
    }
}
