﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class alternativa_minijogo_inspetor : MonoBehaviour {

    private bool botaoPressionado;
    private bool botaoHabilitado;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (botaoPressionado == false && botaoHabilitado == true)
        {
            Vector2 p = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D[] hits = Physics2D.OverlapPointAll(p);
            for (int k = 0; k < hits.Length; k++)
            {
                if (hits[k].gameObject == this.gameObject)
                {
                    //o mouse está em cima do inspetor
                    Debug.Log("alternativa_minijogo_inspetor.cs//mouse em cima de alternativa");

                    if (Input.GetMouseButton(0))
                    {
                        for (int i = 1; i < 5; i++)
                        {
                            GameObject objetoAlternativa = GameObject.Find("alterativa_inspetor_" + i);
                            objetoAlternativa.GetComponent<alternativa_minijogo_inspetor>().desabilitarBotao();
                        }
                        botaoPressionado = true;
                        string respostaUsuario = gameObject.GetComponent<Text>().text.Replace("> ", "");
                        GameObject.Find("inspetor_comecar_minijogo").GetComponent<inspetor_script>().usuarioSelecionouAlternativa(respostaUsuario, gameObject);

                    }

                }
            }
        }

    }

    public void desabilitarBotao()
    {
        botaoHabilitado = false;
    }

    void OnEnable()
    {
        botaoPressionado = false;
        botaoHabilitado = true;
    }

        void OnMouseDown()
    {
        

    }
}
